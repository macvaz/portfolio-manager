#!/bin/bash

NAME="portfolio-manager"
VENV=/home/ubuntu/venvs/$NAME
export PROJECT_HOME=/home/ubuntu/projects/$NAME

# Activate the virtual environment
source $VENV/bin/activate
export PYTHONPATH=$PROJECT_HOME:$PYTHONPATH
export DJANGO_SETTINGS_MODULE=portfolio-manager.settings

cd $PROJECT_HOME
./manage.py navs
