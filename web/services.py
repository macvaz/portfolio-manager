# -*- coding: utf-8 -*-

'''
Created on 30/11/2012

@author: macvaz
'''

from StringIO import StringIO

from django.http import HttpResponse
from django.contrib.auth.decorators import login_required

from db import queries
from models import Favorite

from analysis.constants import constants

from datetime import datetime

import json

_ERROR_MISSING_MONEY_FUND = 'One Money Market fund should exist, {0} found.'
_ERROR_WRONG_NUMBER_FUNDS = 'A minimum of 3 funds and a maximum of 7 funds required.'

##############
# POOL  #
##############


@login_required
def toggle_pool(request, manager, isin):
    result = queries.toggle_pool(manager, isin, request.user)

    return HttpResponse(json.dumps(result), content_type='text/json')


@login_required
def get_AAA_managers(request):
    managers = [manager.to_json() for manager in queries.get_AAA_asset_managers()]

    return HttpResponse(json.dumps(managers), content_type='text/json')


@login_required
def get_AAA_pool(request):
    pools = [pool.to_json() for pool in queries.get_AAA_pool_by_user(request.user)]

    return HttpResponse(json.dumps(pools), content_type='text/json')


@login_required
def get_funds_by_manager(request, manager):
    metrics = [fund.to_json() for fund in queries.get_metrics_by_manager(manager)]

    return HttpResponse(json.dumps(metrics), content_type='text/json')

############
# SIGNALS  #
############


def get_signals(request):
    signals = queries.get_signals()

    return HttpResponse(json.dumps(signals), content_type='text/json')


def get_momentum(request, category, currency):
    momentum = queries.get_ranking(category, currency)

    return HttpResponse(json.dumps(momentum), content_type='text/json')

############
# METRICS  #
############


@login_required
def get_historic_metrics(request, category, currency, rule, order_by, direction, month, year):
    date = _get_date(1, month, year)

    funds = queries.get_funds_by_rule(category, currency, rule, order_by, direction, [date], None)

    return HttpResponse(json.dumps(funds), content_type='text/json')


@login_required
def get_latest_metrics(request, category, currency, rule, order_by, direction, subcategory):
    dates = queries.get_latest_n_dates(15)

    funds = queries.get_funds_by_rule(category, currency, rule, order_by, direction, dates, subcategory)

    return HttpResponse(json.dumps(funds), content_type='text/json')

##############
# FAVORITES  #
##############


@login_required
def favorites(request):
    favorites = queries.get_favorites(request.user)

    return HttpResponse(json.dumps(favorites), content_type='text/json')


@login_required
def curve(request):
    portfolio = Favorite.objects.filter(user=request.user, weight__gt=0)
    equity_curve_ser, daily_returns_indexes = queries.get_portfolio_equity_curve(portfolio)
    formatted_equity_curve = _adapt_to_chartjs_format(equity_curve_ser)
    result = {
        "portfolio": formatted_equity_curve,
    }

    for isin, series in daily_returns_indexes.items():
        result[isin] = _adapt_to_chartjs_format(series)

    return HttpResponse(json.dumps(result), content_type='text/json')


@login_required
def set_weight(request, isin, weight):
    fav = Favorite.objects.get(user=request.user, fund_id=isin)
    fav.weight = int(weight)/100.0
    fav.save()

    return HttpResponse(json.dumps({'res': 'OK'}), content_type='text/json')


@login_required
def add_favorite(request, isin):
    result = queries.add_favorite(request.user, isin)

    return HttpResponse(json.dumps(result), content_type='text/json')


@login_required
def delete_favorite(request, isin):
    result = queries.delete_favorite(request.user, isin)

    return HttpResponse(json.dumps(result), content_type='text/json')


@login_required
def favorite_metrics(request):
    result = queries.get_favorite_metrics(request.user)

    return HttpResponse(json.dumps([result]), content_type='text/json')

###################
# RULES DROPDOWN  #
###################


@login_required
def params(request):
    params = {
        'categories': constants.get_categories(),
        'currencies': constants.get_currencies(),
        'screener_rules': constants.get_screener_rules()
    }

    return HttpResponse(json.dumps(params), content_type='text/json')

#####################
# PRIVATE FUNCTIONS #
#####################


def _adapt_to_chartjs_format(series):
    formatted_list = [{"t": k.strftime("%Y-%m-%d"), "y": round(v, 2)} for k, v in series.to_dict().items()]
    formatted_list.sort(key=lambda k: k["t"])
    return formatted_list


def _get_date(day, month, year):
    return datetime(int(year), int(month), int(day))


def _check_simulation(funds):
    num_money = 0
    cash = None

    if len(funds) > 7 or len(funds) < 3:
        return _ERROR_WRONG_NUMBER_FUNDS, None

    for fund in funds:
        if fund.category == "MON":
            num_money += 1

            if cash is None:
                cash = fund

    if num_money != 1:
        return _ERROR_MISSING_MONEY_FUND.format(num_money), None

    return None, cash