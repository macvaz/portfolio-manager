from django.conf.urls import patterns

import views
import services

urlpatterns = patterns(
    '',

    #########
    # VIEWS #
    #########

    (r'^$', views.index),
    (r'^screener$', views.screener),
    (r'^portfolios$', views.portfolios),
    (r'^simulation$', views.simulation),
    (r'^ranking$', views.ranking),

    ############
    # SERVICES #
    ############

    (r'^service/favorites$', services.favorites),
    (r'^service/favorites/curve$', services.curve),
    (r'^service/favorites/add/(?P<isin>\w+)$', services.add_favorite),
    (r'^service/favorites/del/(?P<isin>\w+)$', services.delete_favorite),
    (r'^service/favorites/metrics$', services.favorite_metrics),
    (r'^service/favorites/weight/(?P<isin>\w+)/(?P<weight>\d+)$', services.set_weight),
    (r'^service/params$', services.params),

    (r'^service/funds/(?P<category>\w+)/(?P<currency>\w+)/(?P<rule>\d+)/(?P<order_by>\w+)/(?P<direction>[+|-])/(?P<month>\d{1,2})/(?P<year>\d{4})$', services.get_historic_metrics),
    (r'^service/funds/(?P<category>\w+)/(?P<currency>\w+)/(?P<rule>\d+)/(?P<order_by>\w+)/(?P<direction>[+|-])/(?P<subcategory>\d+)/latest_metrics$', services.get_latest_metrics),

    (r'^service/momentum/(?P<category>\w+)/(?P<currency>\w+)$', services.get_momentum),
    (r'^service/signals$', services.get_signals),

    # AAA (adaptative asset allocation)
    (r'^service/aaa/companies$', services.get_AAA_managers),
    (r'^service/aaa/pool$', services.get_AAA_pool),
    (r'^service/aaa/companies/(?P<manager>\d+)/funds$', services.get_funds_by_manager),
    (r'^service/aaa/companies/(?P<manager>\d+)/toggle/(?P<isin>\w+)$', services.toggle_pool),

   #########
   # USERS #
   #########

    (r'^logout/$', 'django.contrib.auth.views.logout',{'next_page': '/'})
)
