'''
Created on 27/03/2013

@author: macvaz
'''

from django.db import models
from django.contrib import admin

from django.contrib.auth.models import User

from analysis.models import Fund, AssetManager


class Favorite(models.Model):
    fund = models.ForeignKey(Fund)
    user = models.ForeignKey(User)
    weight = models.FloatField(default=0.0)

    class Meta:
        db_table = 'favorite'


class Pool(models.Model):
    fund = models.ForeignKey(Fund)
    company = models.ForeignKey(AssetManager)
    user = models.ForeignKey(User)

    def to_json(self):
        return {
            'isin': unicode(self.fund),
            'company': self.company.id
        }

    class Meta:
        db_table = 'pool'
        unique_together = (("fund", "company"),)

# Registering in Admin
admin.site.register(Favorite)
admin.site.register(Pool)
