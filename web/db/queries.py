'''
Created on 24/03/2013

@author: mac
'''
import datetime
from django.db.models import Q

import numpy as np

from analysis.models import Fund, PerformanceMetrics, Ranking, AssetManager
from analysis.tools import portfolio_metrics
from analysis.constants import constants
from web.models import Favorite, Pool
from analysis.algos.toolkit import ret_log_portfolio

from analysis.db import csv
from analysis.algos import toolkit


#############
# CONSTANTS #
#############

_MONTHS = ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec']

_RULES = {
    '0': {'sharpe_3m__gt': 1.0, 'sharpe_1m__gt': 1.0, 'sharpe_6m__gt': 1.0},
    '1': {'sharpe_6m__gt': 1.0},
    '2': {'sharpe_3m__gt': 1.0},
    '3': {'sharpe_1m__gt': 1.0},
    '4': {'performance_1m__gt': 0, 'volatility_6m__gt': 1.0},
    '5': {'performance_1d__gt': 0},
    '6': {'performance_2d__gt': 0},
    '7': {'performance_3d__gt': 0},
    '8': {'performance_1w__gt': 0},
    '9': {'performance_2d__gt': 0, 'performance_1w__gt': 0},
    '10': {}
}

####################
# PUBLIC FUNCTIONS #
####################


def get_all_favorite_funds():
    favs = Favorite.objects.all().order_by('fund__name')
    return [fav.fund for fav in favs]


def get_favorite_funds(user):
    favs = Favorite.objects.filter(user=user).order_by('fund__name')
    return [fav.fund for fav in favs]


def get_AAA_asset_managers():
    return AssetManager.objects.order_by('name')


def get_AAA_pool_by_user(user):
    """
    Returns a list of Pool objects
    @param user: auth.User
    @return: [models.Pool]
    """
    return Pool.objects.filter(user=user)


def get_AAA_pool_by_user_and_manager(manager_id, user):
    """
    Returns a list of Pool objects
    @param manager_id: int
    @param user: auth.User
    @return: [models.Pool]
    """
    return Pool.objects.filter(user=user, company__id=manager_id)


def get_metrics_by_manager(manager_id):
    return PerformanceMetrics.objects.filter(fund__company__id=manager_id).order_by('fund__category')


def get_funds_by_manager(manager_id):
    return Fund.objects.filter(company__id=manager_id).order_by('name')


def get_latest_n_dates(num_days):
    date_objects = PerformanceMetrics.objects.order_by('-date').values('date').distinct()

    n = min(len(date_objects), num_days)

    return [date_object['date'] for date_object in date_objects[0:n]]


def get_latest_metric(fund):
    return PerformanceMetrics.objects.filter(fund=fund).latest('date')


def get_funds_by_rule(category, currency, rule, order_by, direction, date_list, subcategory):
    if direction == '+':
        direction = ''

    general_dict = {
        'fund__category': category,
        'fund__currency': currency,
        'date__in': date_list,
    }

    if int(subcategory) > 0:
        general_dict['fund__subcategory'] = subcategory

    rule_dict = _RULES[rule]

    kwargs = {}

    kwargs.update(general_dict)
    kwargs.update(rule_dict)

    metrics = PerformanceMetrics.objects.filter(**kwargs).order_by('{0}{1}'.format(direction, order_by))[:99]

    return [metric.to_json() for metric in metrics]


def get_signals():
    signals = Ranking.objects.filter(Q(exit=True) | Q(entry=True)).order_by('-entry')

    return [signal.to_json() for signal in signals]


def get_ranking(category, currency):
    signals = Ranking.objects.filter(fund__category=category, fund__currency=currency,
                                     ranking__gt=1).order_by('-ranking')[:99]

    return [signal.to_json() for signal in signals]


def get_favorites(user):
    favs = Favorite.objects.filter(user=user).order_by('fund__name')

    funds = [fav.fund.isin for fav in favs]
    metrics = [metric.to_json() for metric in PerformanceMetrics.objects.filter(fund__in=funds)]

    portfolio_positions = get_ordered_positions(user)
    portfolio_fund_ids = [position.fund.isin for position in portfolio_positions]
    portfolio_ytd_metrics = [metric.to_json()['performance_ytd'] for metric in PerformanceMetrics.objects.filter(fund__in=portfolio_fund_ids).order_by('fund__isin')]

    portfolio_ytd_performance = round(sum([pos.weight * perf for (pos, perf) in zip(portfolio_positions, portfolio_ytd_metrics)])/100, 4)

    favorites = []

    for (i, fav) in enumerate(favs):
        for metric in metrics:
            if fav.fund_id == metric['isin']:
                metric["weight"] = fav.weight
                metric["portfolio_perfomance_ytd"] = portfolio_ytd_performance
                favorites.append(metric)
                break

        if len(favorites) == i:
            favorites.append(_create_empty_metric(fav.fund_id))

    return favorites


def get_ordered_positions(user):
    return [fav for fav in Favorite.objects.filter(user=user, weight__gt=0).order_by('fund__isin')]


def get_fund_by_isin(isin):
    return Fund.objects.get(isin=isin)


def get_manager_by_id(id):
    return AssetManager.objects.get(id=id)


def get_funds_by_isins(isins):
    return Fund.objects.filter(isin__in=isins)


def add_favorite(user, isin):
    fund = get_fund_by_isin(isin)

    if len(Favorite.objects.filter(user=user, fund=fund)) == 0:
        Favorite(user=user, fund=fund).save()

    return {'result': 'OK'}


def toggle_pool(manager_id, isin, user):
    fund = get_fund_by_isin(isin)
    manager = get_manager_by_id(manager_id)

    pools = Pool.objects.filter(fund=fund, company=manager, user=user)

    if len(pools) == 0:
        pool = Pool(fund=fund, company=manager, user=user)
        pool.save()
    else:
        pool = pools[0]
        pool.delete()

    return {'result': 'OK'}


def delete_favorite(user, isin):
    Favorite.objects.filter(user=user, fund=Fund.objects.get(isin=isin)).delete()

    return {'result': 'OK'}


def get_favorite_metrics(user):
    portfolio = Favorite.objects.filter(user=user, weight__gt=0)
    corr = _get_portfolio_correlation(portfolio, 6, constants.BENCHMARK)
    beta = _get_portfolio_beta(portfolio)
    navs, weights = _read_from_files(portfolio)
    vol_1y = _get_portfolio_volatility(navs, weights, 12)
    vol_6m = _get_portfolio_volatility(navs, weights, 6)
    vol_3m = _get_portfolio_volatility(navs, weights, 3)
    vol_1m = _get_portfolio_volatility(navs, weights, 1)
    ytd = _get_ytd_performance(navs, weights)
    ret_1y = _get_portfolio_performance(portfolio, '12m')
    ret_9m = _get_portfolio_performance(portfolio, '9m')
    ret_6m = _get_portfolio_performance(portfolio, '6m')
    ret_3m = _get_portfolio_performance(portfolio, '3m')
    ret_1m = _get_portfolio_performance(portfolio, '1m')
    ret_2w = _get_portfolio_performance(portfolio, '2w')
    ret_1w = _get_portfolio_performance(portfolio, '1w')

    sharpe_1m = ret_1m / vol_1m

    return {
        'volatility': vol_6m,
        'beta': beta,
        'corr': corr,
        'weight': sum([fav.weight for fav in portfolio]),
        'performance_1w': ret_1w,
        'performance_2w': ret_2w,
        'performance_1m': ret_1m,
        'performance_3m': ret_3m,
        'performance_6m': ret_6m,
        'performance_9m': ret_9m,
        'performance_1y': ret_1y,
        'performance_ytd': ytd,
        'sharpe_1m': sharpe_1m if not np.isnan(sharpe_1m) else None,
        'sharpe_3m': ret_3m / vol_3m,
        'sharpe_6m': ret_6m / vol_6m,
        'sharpe_1y': ret_1y / vol_1y
    }
#####################
# PRIVATE FUNCTIONS #
#####################


def _get_portfolio_performance(favs, period):
    """
    Compute portfolio performance
    :param favs: [model.Favorite]
    :params period: '1m', "1w'
    :return: float
    """
    isins = [f.fund_id for f in favs]
    metrics = PerformanceMetrics.objects.filter(fund__in=isins)

    performance_label = 'performance_{}'.format(period)
    period_performances = map(lambda metric:
        {'fund_id': metric.fund_id,
        'performance': getattr(metric, performance_label)},
        metrics)

    valid_metrics = filter(lambda m: m['performance'] is not None, period_performances)
    weights = {f.fund_id: f.weight for f in favs}

    if len(metrics) != len(favs):
        return 'NA'

    try:
        return sum([metric['performance']*weights[metric['fund_id']]/100 for metric in valid_metrics])
    except Exception:
        print 'Error computing portfolio performance'
        return None


def _get_ytd_performance(navs, weights):
    ytd_navs = toolkit.ytd_navs(navs)

    daily_returns = ret_log_portfolio(ytd_navs, None)
    w = np.array(weights)

    return (daily_returns.sum()*w.T).sum()


def _get_portfolio_volatility(navs, weights, months):
    """
    Compute portfolo volatility of the last 'months' months
    :param favs: [model.Favorite]
    :return: float
    """
    return portfolio_metrics.volatility(navs, weights, months)


def _read_from_files(favs):
    isins = [fav.fund_id for fav in favs]
    weights = [fav.weight for fav in favs]
    navs = csv.read_navs(isins, None, None)

    return navs, weights


def _get_portfolio_correlation(favs, months, benchmark_isin):
    """
    Compute portfolo correlation of the last 'months' months
    :param favs: [model.Favorite]
    :param months: int
    :return: float
    """
    isins = [fav.fund_id for fav in favs]
    weights = [fav.weight for fav in favs]
    navs = csv.read_navs(isins, benchmark_isin, constants.BENCHMARK_COLUMN_NAME)
    return portfolio_metrics.correlation(navs, weights, months, constants.BENCHMARK_COLUMN_NAME)


def get_portfolio_equity_curve(porfolio):
    """
    Compute portfolo correlation of the last 'months' months
    :param porfolio: [model.Favorite]
    :return: pd.Series
    """
    portfolio_isins = [fav.fund_id for fav in porfolio]
    weights = [fav.weight for fav in porfolio]
    navs_df = csv.read_navs_several_benchmarks(portfolio_isins, constants.GRAPH_BENCHMARKS)
    daily_returns_with_benchmarks_df = navs_df.pct_change()[1:]

    # Portfolio
    daily_returns_df = daily_returns_with_benchmarks_df[portfolio_isins]
    weighted_returns = (weights * daily_returns_df)
    equity_return = weighted_returns.sum(axis=1)
    equity_curve_portfolio = 100*(1 + equity_return/100).cumprod()
    # Benchmarks
    benchmark_equity_curves = {isin: _compute_benchmark_equity_curve(daily_returns_with_benchmarks_df, isin) for isin in constants.GRAPH_BENCHMARKS}

    return equity_curve_portfolio, benchmark_equity_curves


def _compute_benchmark_equity_curve(daily_returns, benchmark_isin):
    daily_returns_index_ser = daily_returns[benchmark_isin]
    return 100 * (1 + daily_returns_index_ser).cumprod()


def _get_portfolio_beta(favs):
    """
    Compute 6m portfolo beta
    :param favs: [model.Favorite]
    :return: float
    """
    isins = [f.fund_id for f in favs]
    metrics = PerformanceMetrics.objects.filter(fund__in=isins)
    weights = {f.fund_id: f.weight for f in favs}

    if len(metrics) != len(favs):
        return 'NA'

    return sum([metric.beta_6m*weights[metric.fund_id] for metric in metrics])/100.0


def _create_empty_metric(isin):
    fund = get_fund_by_isin(isin)

    return {
        'name': fund.name,
        'isin': fund.isin_id,
        'url': fund.get_url(),

        'performance_1d': None,
        'performance_2d': None,
        'performance_3d': None,

        'performance_1w': None,
        'performance_2w': None,
        'performance_3w': None,

        'performance_1m': None,
        'performance_2m': None,
        'performance_3m': None,
        'performance_6m': None,
        'performance_12m': None,

        'sharpe_1m': None,
        'sharpe_3m': None,
        'sharpe_6m': None,
        'sharpe_12m': None,

        'volatility_1m': None,
        'volatility_3m': None,
        'volatility_6m': None,
        'volatility_12m': None,

        'category': fund.category
    }
