'''
Created on 30/11/2012

@author: macvaz
'''

from django.contrib.auth.decorators import login_required
from django.shortcuts import render


@login_required
def index(request):
    return portfolios(request)


@login_required
def screener(request):
    context = {
        'active': 'screener',
        'viewmodel': 'Screener'
    }

    return render(request, 'screener.html', context)


@login_required
def portfolios(request):
    context = {
        'active': 'favorites',
        'viewmodel': 'Favorites'
    }

    return render(request, 'favorites.html', context)


@login_required
def simulation(request):
    context = {
        'active': 'simulation',
        'viewmodel': 'Simulation'
    }

    return render(request, 'simulation.html', context)


@login_required
def ranking(request):
    context = {
        'active': 'ranking',
        'viewmodel': 'Ranking'
    }

    return render(request, 'ranking.html', context)
