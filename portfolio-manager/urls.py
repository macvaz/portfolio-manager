from django.conf.urls import patterns, include, url

import settings

from django.contrib.auth.views import login, logout

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    
    (r'^accounts/login/$',  login),
    (r'^accounts/logout/$', logout),

    url(r'', include('web.urls')),
    
    url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT, 'show_indexes':True})
)
