function Favorites_ViewModel() {
	var self = this;

    ///////////////////////////////////////////////////
    // OBSERVABLE DATA
    ///////////////////////////////////////////////////

    self.funds = ko.observableArray([]);
    self.isin  = ko.observable();

    // Data for chartjs
    self.equity_curve = [];
    self.msci_world = [];
    self.sp500 = [];
    self.chart = null;

    // Money amount of the total portfolio
    self.money = ko.observable(100000);

    self.portfolio_funds = ko.computed(function(){
        return ko.utils.arrayFilter(self.funds(), self.filter_portfolio_fund);
    });

    self.favorite_funds = ko.computed(function(){
        return ko.utils.arrayFilter(self.funds(), self.filter_favorite_fund);
    });

    self.portfolio_metrics = ko.observableArray([]);

    self.section_name = ko.observable('Management');
    self.section_code = ko.observable('mngt');

    self.filter_portfolio_fund = function(fund) {
        return fund.weight() > 0;
    };

    self.filter_favorite_fund = function(fund) {
        return fund.weight() <= 0;
    };

    self.ready = false;
    self.portfolio_performance_ytd = 0;

    ///////////////////////////////////////////////////
    // METHODS
    ///////////////////////////////////////////////////

    self.availableIsin = ko.computed(function() {
        return self.isin() && self.isin().length > 4;
    });

    self.changeSection = function (viewModel, event) {
        var target = $(event.target);

        var code = target.attr('data-code');
        var text = target.html();

        var tabs = target.parent().siblings();
        tabs.removeClass('active');

        target.parent().addClass("active");

        self.section_code(code);
        self.section_name(text);
    };
    
    self.loadFunds = function () {
    	var url = "/service/favorites";
    	
        $.getJSON(url, function(funds) {
        	var mappedFunds = $.map(funds, function(item) { return new Fund(item, self) });

        	self.portfolio_performance_ytd = mappedFunds[0].portfolio_performance_ytd
            
        	self.funds(mappedFunds);
            self.ready = true;
            self.loadPortfolioMetrics();
            self.addTooltips();
        })  
    };

    self.addTooltips = function () {
        $('.weights .weight-input').tooltip({
            title: self.hoverTooltipData,
            html: true,
            container: 'body',
        });
    }

    self.hoverTooltipData = function () {
        var element = $(this);
        return Math.round(element[0].value/100*self.money());
    }

    self.loadEquityCurve = function () {
    	var url = "/service/favorites/curve";

        $.getJSON(url, function(result) {
        	self.equity_curve = result["portfolio"];
        	self.msci_world = result["LU0996182563"];
        	self.sp500 = result["LU0996179007"];
        	self.showCanvas();
        })
    };

    self.loadPortfolioMetrics = function () {
    	var url = "/service/favorites/metrics";

        $.getJSON(url, function(funds) {
        	var metric = $.map(funds, function(item) { return new PortfolioMetric(item, self, self.portfolio_performance_ytd) });
        	self.portfolio_metrics(metric);
        })
    };
    
    self.addByISIN = function (event) {
    	var url = "/service/favorites/add/" + self.isin();
    	
        $.getJSON(url, function(funds) {
        	self.loadFunds();
        	self.loadEquityCurve();
        	self.isin('');
        })  
    };
    
    ///////////////////////////////////////////////////
    // INIT
    ///////////////////////////////////////////////////

    self.init = function () {
        self.loadFunds();
        self.loadEquityCurve();
    };

    self.showCanvas = function () {
        if (self.chart) {
            self.chart.destroy();
        }

        var ctx = document.getElementById('equity_canvas').getContext('2d');
		ctx.canvas.width = 1000;
		ctx.canvas.height = 300;

		var color = Chart.helpers.color;
		var cfg = {
			data: {
				datasets: [
				    {
                        label: 'Portfolio',
                        backgroundColor: 'rgba(255, 99, 132, 0.2)',
                        borderColor: 'rgba(255, 99, 132, 1)',
                        data: self.equity_curve,
                        type: 'line',
                        pointRadius: 0,
                        fill: false,
                        lineTension: 0,
                        borderWidth: 2
				    },
				    {
                        label: 'MSCI World',
                        backgroundColor: 'rgba(54, 162, 235, 0.2)',
                        borderColor: 'rgba(54, 162, 235, 1)',
                        data: self.msci_world,
                        type: 'line',
                        pointRadius: 0,
                        fill: false,
                        lineTension: 0,
                        borderWidth: 2
				    }/*,
				    {
                        label: 'MSCI SP500',
                        backgroundColor: 'rgba(255, 205, 86, 0.2)',
                        borderColor: 'rgba(255, 205, 86, 1)',
                        data: self.sp500,
                        type: 'line',
                        pointRadius: 0,
                        fill: false,
                        lineTension: 0,
                        borderWidth: 2
				    }*/

				]
			},
			options: {
				scales: {
					xAxes: [{
						type: 'time',
						time: {
                            unit: 'month'
                        },
						offset: true,
						ticks: {
                            minRotation: 20
                        }
					}],
					yAxes: [{
						gridLines: {
							drawBorder: true
						}
					}]
				},
				tooltips: {
					mode: 'index'
				}
			}
		};

		self.chart = new Chart(ctx, cfg);

    }

    self.init();
}


