function Ranking_ViewModel() {
	
	var self = this;

    ///////////////////////////////////////////////////
    // OBSERVABLE DATA
    ///////////////////////////////////////////////////

    self.categories = ko.observableArray([]);
    self.currencies = ko.observableArray([]);
    
    self.category = ko.observable();
    self.currency = ko.observable();

    self.section_name = ko.observable('Ranking');
    self.section_code = ko.observable('ranking');
    
    self.rankings = ko.observableArray([]);

    ///////////////////////////////////////////////////
    // METHODS
    ///////////////////////////////////////////////////s

    self.changeSection = function (viewModel, event) {
        var target = $(event.target);

    	var code = target.attr('data-code');
        var text = target.html();

        var tabs = target.parent().siblings();
        tabs.removeClass('active');

        target.parent().addClass("active");

        self.section_code(code);
        self.section_name(text);
    };
    
    self.highlightColumn = function (selected) {
    	$('.funds th').removeClass("selected");
    	$(selected).toggleClass("selected");
    }
    
    self.reload = function () {
    	var url = "/service/momentum/" + self.category().code() + "/" + self.currency().code();
    	
        $.getJSON(url, function(ranking) {
        	var mappedRankings = $.map(ranking, function(item) { return new Ranking(item, self) });
            
        	self.rankings(mappedRankings);
        });  
    };

    self.loadParams = function () {
        var url = "/service/params";

        $.getJSON(url, function(searchParams) {
        	// Categories
        	var categories = searchParams['categories'];
        	categories = $.map(categories, function(item) { return new SearchParam(item, self) });

        	self.categories(categories);
        	self.category(categories[0]);

        	// Currencies
        	var currencies = searchParams['currencies'];
        	currencies = $.map(currencies, function(item) { return new SearchParam(item, self) });

        	self.currencies(currencies);
        	self.currency(currencies[0]);

        	// Loading rankings
        	self.reload();
        });
    }
    
    ///////////////////////////////////////////////////
    // INIT
    ///////////////////////////////////////////////////
    
    self.init = function () {
        self.loadParams();
    };
    
    self.init();
}