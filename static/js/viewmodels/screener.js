function Screener_ViewModel() {
	
	var self = this;

    ///////////////////////////////////////////////////
    // OBSERVABLE DATA
    ///////////////////////////////////////////////////

    self.categories = ko.observableArray([]);
    self.currencies = ko.observableArray([]);
    self.rules      = ko.observableArray([]);
    
    self.category = ko.observable();
    self.currency = ko.observable();
    self.rule     = ko.observable();

    self.section_name = ko.observable('Short-term');
    self.section_code = ko.observable('short');
    
    self.order_by  = ko.observable('sharpe_3m');
    self.direction = ko.observable('-');
    
    self.funds = ko.observableArray([]);
    self.subcategory = ko.observable(0);
    self.subcategories = [];

    ///////////////////////////////////////////////////
    // METHODS
    ///////////////////////////////////////////////////s

    self.changeSection = function (viewModel, event) {
        var target = $(event.target);

    	var code = target.attr('data-code');
        var text = target.html();

        var tabs = target.parent().siblings();
        tabs.removeClass('active');

        target.parent().addClass("active");

        self.section_code(code);
        self.section_name(text);
    };
    
    self.changeOrderBy = function (viewModel, event) {
    	var code      = $(event.target).attr('data-code');
    	var direction = $(event.target).attr('data-direction');
    	
    	self.order_by(code);
    	self.direction(direction);
    	
    	self.highlightColumn(event.target);
    	
    	self.reload();
    };
    
    self.highlightColumn = function (selected) {
    	$('.funds th').removeClass("selected");
    	$(selected).toggleClass("selected");
    }
    
    self.reload = function () {
    	
    	var url = "/service/funds/" + self.category().code() + "/" + self.currency().code() + "/" + 
    		      self.rule().code() + "/" + self.order_by() + "/" + self.direction() + "/" +
                  self.subcategory() + "/latest_metrics";

        $.getJSON(url, function(funds) {
        	var mappedFunds = $.map(funds, function(item) { return new Fund(item, self) });
            
        	self.funds(mappedFunds);

            var rawSubcategories = mappedFunds.map(function(fund){
              return {name: fund.ms_category(), id: fund.subcategory_id()};
            });

            var subcategories = {};
            rawSubcategories.forEach(function (rawSubcategory) {
                var name = rawSubcategory.name;
                var subcategory = subcategories[name];

                if (!subcategory) {
                    subcategories[name] = {'name': name, count: 1, id: rawSubcategory.id};
                } else {
                    subcategory.count++;
                }
            });

            subcategories = Object.keys(subcategories).map(function (name) {
                return subcategories[name];
            });

            self.showTags(subcategories);

            self.subcategories = subcategories;
        });  
    };

    self.showTags = function (subcategories) {
        $('.tag-cloud svg').remove();

        var size = subcategories.length == 1 ? subcategories[0].count*2 : subcategories.length;
        var fill = d3.scale.category20();

        d3.layout.cloud().size([600, 300])
            .words(subcategories.map(function(d) {
            return {text: d.name, size: 15 + 2.5 * d.count/size*10};
            }))
            .rotate(function() { return ~~(Math.random() * 6); })
            .font("Impact")
            .fontSize(function(d) { return d.size; })
            .on("end", draw)
            .start();

        function draw(words) {
            d3.select(".tag-cloud").append("svg")
                .attr("width", 600)
                .attr("height", 300)
              .append("g")
                .attr("transform", "translate(300,150)")
              .selectAll("text")
                .data(words)
              .enter().append("text")
                .style("font-size", function(d) { return d.size + "px"; })
                .style("font-family", "Impact")
                .style("fill", function(d, i) { return fill(i); })
                .attr("text-anchor", "middle")
                .attr("transform", function(d) {
                  return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
                })
                .text(function(d) { return d.text; })
                .style("cursor", "pointer")
                .on("click", function(d) {
                    var subcategory = self.getSubcategoryId(d.text);

                    if (subcategory === self.subcategory()) {
                        self.subcategory(0);
                    } else {
                        self.subcategory(subcategory);
                        console.log(subcategory);
                    }

                    self.reload();
                });
        }
    };

    self.getSubcategoryId = function (name) {
        for (var i=0; i<self.subcategories.length; i++) {
            var subcategory = self.subcategories[i];

            if (subcategory.name === name) {
                return subcategory.id;
            }
        }
        return '';
    };

    self.loadParams = function () {
        var url = "/service/params";

        $.getJSON(url, function(searchParams) {
        	// Categories
        	var categories = searchParams['categories'];
        	categories = $.map(categories, function(item) { return new SearchParam(item, self) });

        	self.categories(categories);
        	self.category(categories[0]);

        	// Currencies
        	var currencies = searchParams['currencies'];
        	currencies = $.map(currencies, function(item) { return new SearchParam(item, self) });

        	self.currencies(currencies);
        	self.currency(currencies[0]);

        	// Rules
        	var rules = searchParams['screener_rules'];
        	rules = $.map(rules, function(item) { return new SearchParam(item, self) });

        	self.rules(rules);
        	self.rule(rules[0]);

        	// Loading funds
        	self.reload();
        });
    }
    
    ///////////////////////////////////////////////////
    // INIT
    ///////////////////////////////////////////////////
    
    self.init = function () {
        self.loadParams();
    };
    
    self.init();
}