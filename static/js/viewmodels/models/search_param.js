function SearchParam(data, root) {
	
	var self = this;
	
	self.root = root; 
	
    self.code  = ko.observable(data.code);
	self.name  = ko.observable(data.name);
	
	///////////////////////////////////////////////////
    // OPERATIONS
    ///////////////////////////////////////////////////
    
    self.changeCategory = function (category, event) {
    	self.root.category(category);
    	
    	self.root.reload();
    };

    self.changeManager = function (manager, event) {
        self.root.manager(manager);

        self.root.reload();
    };
    
    self.changeCurrency = function (currency, event) {
    	self.root.currency(currency);
    	
    	self.root.reload();
    };
    
    self.changeRule = function (rule, event) {
    	if (rule.code() == 4) {
    		self.root.order_by('performance_1m');
    		self.root.highlightColumn($('.funds th[data-code="performance_1m"]'));
    	} else {
    		self.root.order_by('sharpe_3m');
    		self.root.highlightColumn($('.funds th[data-code="sharpe_3m"]'));
    	}
    	
    	self.root.direction('-');
    	
    	self.root.rule(rule);
    	
    	self.root.reload();
    };
	
}