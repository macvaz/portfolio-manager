function MonthlyResult(data, root) {
	
	var self = this;
	
	self.round_1 = function (number) {
		if (!number || number == 0.0)
			return '';
		
		return Math.round(number * 10) / 10;
	};

    self.percentage = function (number) {
        return number() !== '' ? Math.abs(number()) + ' %' : '';
    };

    self.perfCss = function (month) {
		var color = month() >= 0 ? 'green ' : 'red ';

		return color;
	};
	
	self.root = root;
 
    self.jan  = ko.observable(self.round_1(data.Jan));
    self.feb  = ko.observable(self.round_1(data.Feb));
    self.mar  = ko.observable(self.round_1(data.Mar));
    self.apr  = ko.observable(self.round_1(data.Apr));
    self.may  = ko.observable(self.round_1(data.May));
    self.jun  = ko.observable(self.round_1(data.Jun));
    self.jul  = ko.observable(self.round_1(data.Jul));
    self.aug  = ko.observable(self.round_1(data.Aug));
    self.sep  = ko.observable(self.round_1(data.Sep));
    self.oct  = ko.observable(self.round_1(data.Oct));
    self.nov  = ko.observable(self.round_1(data.Nov));
    self.dec  = ko.observable(self.round_1(data.Dec));

    self.year  = ko.observable(self.round_1(data.Year));
    self.maxDD = ko.observable(self.round_1(data.MaxDD));

    self.date  = ko.observable(data.date);
}