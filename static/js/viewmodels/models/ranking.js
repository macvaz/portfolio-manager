function Ranking(data, root) {
	
	var self = this;
	
	self.root = root;

    self.percentage = function (value) {
        value = value();

		if (!value || value == '-')
			return "-";

		return Math.round(value * 10000) / 100 + "%";
    }

    self.markTrue = function (value) {
        value = value();

        return value ? value : '';
    }

    self.ranking = ko.observable(data.ranking);

    self.sr = ko.observable(data.sr);
    self.vol = ko.observable(data.vol);
    self.ret = ko.observable(data.ret);

    self.nav = ko.observable(data.nav + ' ' + data.currency);

    self.mom_cur = ko.observable(data.mom_cur);
    self.mom_prev = ko.observable(data.mom_prev);

    self.entry = ko.observable(data.entry);
    self.exit = ko.observable(data.exit);

    self.url = ko.observable(data.url);

    var name = data.name.substring(0,60);
    self.name = ko.observable(data.name.length <= 60 ? name : name + '...');
}