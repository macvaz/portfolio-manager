function Position(data, totals, root) {
	
	var self = this;
	
	self.round_2 = function (number) {
		if (!number)
			return '-';
		
		return Math.round(number * 100) / 100;
	};
	
	self.root = root;
	
    self.url  = ko.observable(data.url);
	self.name = ko.observable(data.name);

	self.category = ko.observable(data.category);

    self.shares = ko.observable(data.shares);
    self.nav    = ko.observable(data.nav);
    self.value  = ko.observable(data.value);
    self.date   = ko.observable(data.date);
    self.weight = ko.observable(data.weight);

	self.volatility_1m  = ko.observable(self.round_2(data.volatility_1m));
	self.volatility_3m  = ko.observable(self.round_2(data.volatility_3m));
	self.volatility_6m  = ko.observable(self.round_2(data.volatility_6m));
	
	self.percentage = function (attr) {
		var value = self[attr]();
		
		if (!value || value == '-')
			return "";
		
		return value + "%";
    };

    self.euro = function (attr) {
		var value = self[attr]();

		if (!value || value == '-')
			return "";

		return value + "€";
    };

    self.riskCss = function (name, extra) {
		var color = self[name]() >= 1.5 ? 'green ' : 'red ';

		return color + extra;
	};

	self.perfCss = function (name, extra) {
		var color = self[name]() >= 0 ? 'green ' : 'red ';

		return color + extra;
	};

    self.volCss = function (name) {
		var limit = self.getVolLimit();

		return self[name]() <= limit ? 'green' : 'red';
	};

	self.getVolLimit = function () {
		switch (self.category()) {
		case "FIX":
			return 4;
		case "EQT":
			return 12;
		case "MIX":
			return 6;
		case "RE":
			return 12;
		default:
			return 6;
		}
	}

}