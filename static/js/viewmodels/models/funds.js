function Fund(data, root) {
	
	var self = this;
	
	self.round_2 = function (number, extra) {
		if (number === null || number === undefined)
			return '-';

        if (!extra)
            extra = "";
		
		return Math.round(number * 100) / 100 + " " + extra;
	};

    self.round_1 = function (number, extra) {
		if (number === null || number === undefined)
			return 0;

        if (!extra)
            extra = "";

		return Math.round(number * 10) / 10 + " " + extra;
	};

    self.check_null = function (data) {
		if (!data)
			return '-';

		return data;
	};
	
	self.root = root;
   
    self.url  = ko.observable(data.url);

    self.star_status = ko.observable('/static/img/star.png');

    var name = data.name.substring(0,50);

	self.name = ko.observable(data.name.length <= 50 ? name : name + '...');
	self.isin = ko.observable(data.isin);
	
	self.category = ko.observable(data.category);
	
	self.performance_1d  = ko.observable(self.round_2(data.performance_1d));
	self.performance_2d  = ko.observable(self.round_2(data.performance_2d));
	
	self.performance_1w  = ko.observable(self.round_2(data.performance_1w));
	self.performance_2w  = ko.observable(self.round_2(data.performance_2w));
	
	self.performance_1m  = ko.observable(self.round_2(data.performance_1m));
	self.performance_3m  = ko.observable(self.round_2(data.performance_3m));
	self.performance_6m  = ko.observable(self.round_2(data.performance_6m));
    self.performance_9m  = ko.observable(self.round_2(data.performance_9m));
	self.performance_1y  = ko.observable(self.round_2(data.performance_12m));
	self.performance_ytd  = ko.observable(self.round_2(data.performance_ytd));

	// Stored portfolio performance (repeated for every fund) to show it latter
	self.portfolio_performance_ytd  = self.round_2(data.portfolio_perfomance_ytd);
	
	self.sharpe_1m  = ko.observable(self.round_2(data.sharpe_1m));
	self.sharpe_3m  = ko.observable(self.round_2(data.sharpe_3m));
	self.sharpe_6m  = ko.observable(self.round_2(data.sharpe_6m));
    self.sharpe_9m  = ko.observable(self.round_2(data.sharpe_9m));
	self.sharpe_1y  = ko.observable(self.round_2(data.sharpe_12m));
	
	self.volatility_1m  = ko.observable(self.round_2(data.volatility_1m));
	self.volatility_3m  = ko.observable(self.round_2(data.volatility_3m));
	self.volatility_6m  = ko.observable(self.round_2(data.volatility_6m));
    self.volatility_9m  = ko.observable(self.round_2(data.volatility_9m));
	self.volatility_1y  = ko.observable(self.round_2(data.volatility_12m));

    self.weight = ko.observable(self.round_1(data.weight));

    self.beta_6m  = ko.observable(self.round_2(data.beta_6m));
    self.beta_up_6m  = ko.observable(self.round_2(data.beta_up_6m));
    self.beta_down_6m  = ko.observable(self.round_2(data.beta_down_6m));

    self.corr_6m  = ko.observable(self.round_2(data.corr_6m));

    self.ms_category = ko.observable(data.ms_category);
    self.subcategory_id = ko.observable(data.subcategory_id);

    self.markStar = function () {
        self.star_status('/static/img/marked_star.png');
    };

    self.unmarkStar = function () {
        self.star_status('/static/img/star.png');
    };

    self.toggleStar = function () {
        if (self.star_status() === '/static/img/star.png') {
            self.markStar();
            self.addFavorite();
        } else {
            self.unmarkStar();
        }
    };
	
	self.riskCss = function (name, extra) {
		var color = self[name]() >= 0.0 ? 'green ' : 'red ';
		
		return color + extra;
	};
	
	self.perfCss = function (name, extra) {
		var color = self[name]() >= 0 ? 'green ' : 'red ';
		
		return color + extra;
	};
	
	self.percentage = function (performance) {
		var value = self[performance]();
		
		if (!value || value == '-')
			return "-";
		
		return value + "%";
    };

    self.betaCss = function (name, extra) {
		var color = self[name]() <= 0.70 ? 'green ' : 'red ';

		return color + extra;
	};

    self.corCss = function (name, extra) {
		var color = Math.abs(self[name]()) <= 0.75 ? 'red ' : 'green ';

		return color + extra;
	};
	
    self.addFavorite = function (viewModel, event) {
    	var url = "/service/favorites/add/" + self.isin();
    	
        $.getJSON(url, function(response) {
        	
        });  
    };

    self.togglePool = function (viewModel, event) {
        var url = "/service/aaa/companies/" + self.root.manager().code() + "/toggle/" + self.isin();

        $.getJSON(url, function(response) {
            self.toggleStar();
        });
    };
    
    self.delFavorite = function (viewModel, event) {
    	var url = "/service/favorites/del/" + self.isin();
    	
        $.getJSON(url, function(response) {
        	self.root.loadFunds();
        });  
    };
    
	self.volCss = function (name, extra) {
		var limit = self.getVolLimit();
		
		var color = self[name]() <= limit ? 'green' : 'red';

        return color + " " + extra;
	};
	
	self.getVolLimit = function () {
		switch (self.category()) {
		case "FIX": 
			return 4;
		case "EQT": 
			return 12;
		case "MIX": 
			return 6;
		case "RE": 
			return 12;
        case "ALT":
			return 12;
		default:
			return 6;
		}
	};

    self.saveWeight = function (weight, save) {
        if (!root.ready)
            return weight;

        var newWeight = 100*weight();
        var url = "/service/favorites/weight/" + self.isin() + "/" + newWeight;

        $.getJSON(url, function(response) {
            self.root.loadPortfolioMetrics();
            self.root.loadEquityCurve();
        });

        return weight;
    };

}