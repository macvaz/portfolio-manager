function PortfolioMetric(data, root, portfolio_performance_ytd) {
	
	var self = this;
	
	self.round_2 = function (number, extra) {
		if (number === null || number === undefined || isNaN(number))
			return '-';

        if (!extra)
            extra = "";
		
		return Math.round(number * 100) / 100 + " " + extra;
	};

    self.round_1 = function (number, extra) {
		if (number === null || number === undefined)
			return 0;

        if (!extra)
            extra = "";

		return Math.round(number * 10) / 10 + " " + extra;
	};
	
	self.root = root;
	
	self.volatility  = ko.observable(self.round_2(data.volatility));
	self.beta = ko.observable(self.round_2(data.beta));
    self.corr = ko.observable(self.round_2(data.corr));
    self.weight = ko.observable(self.round_1(data.weight));

    self.performance_1w = ko.observable(self.round_2(data.performance_1w));
    self.performance_2w = ko.observable(self.round_2(data.performance_2w));
    self.performance_1m = ko.observable(self.round_2(data.performance_1m));
    self.performance_3m = ko.observable(self.round_2(data.performance_3m));
    self.performance_6m = ko.observable(self.round_2(data.performance_6m));
    self.performance_9m = ko.observable(self.round_2(data.performance_9m));
    self.performance_1y = ko.observable(self.round_2(data.performance_1y));
    self.performance_ytd = ko.observable(self.round_2(portfolio_performance_ytd));

    self.sharpe_1m = ko.observable(self.round_2(data.sharpe_1m));
    self.sharpe_3m = ko.observable(self.round_2(data.sharpe_3m));
    self.sharpe_6m = ko.observable(self.round_2(data.sharpe_6m));
    self.sharpe_1y = ko.observable(self.round_2(data.sharpe_1y));

    self.betaCss = function (name, extra) {
		var color = self[name]() <= 0.70 ? 'green ' : 'red ';

		return color + extra;
	};
    
	self.volCss = function (name, extra) {
		var limit = self.getVolLimit();
		
		var color = self[name]() <= limit ? 'green' : 'red';

        return color + " " + extra;
	};

    self.percentage = function (performance) {
		var value = self[performance]();

		if (!value || value == '-')
			return "-";

		return value + "%";
    };
	
	self.getVolLimit = function () {
		switch (self.category()) {
		case "FIX": 
			return 4;
		case "EQT": 
			return 12;
		case "MIX": 
			return 6;
		case "RE": 
			return 12;
		default:
			return 6;
		}
	};

}