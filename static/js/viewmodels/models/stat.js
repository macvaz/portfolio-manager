function Stat(data, root) {
	
	var self = this;
	
	self.root = root;

	self.round_2 = function (number) {
		if (!number)
			return '-';

		return Math.round(number * 100) / 100;
	};

    self.is_percentage_stat = function (name) {
        return self.NOT_PERCENTAGE_STATS.indexOf(name) == -1;
    };

    self.percentage = function (value) {
		if (!value || value == '-')
			return "-";

		return value + "%";
    };

    self.get_text = function () {
        return self.percentage_stat ? self.percentage(self.value()) : self.value();
    }

    self.NOT_PERCENTAGE_STATS = ['num_years', 'num_months', 'sr'];
 
    self.percentage_stat = self.is_percentage_stat(data.name);

    self.name  = ko.observable(data.name);
	self.value = ko.observable(self.percentage_stat ?
                               self.round_2(data.value * 100) : self.round_2(data.value))
}