function Simulation_ViewModel() {
	
	var self = this;

    ///////////////////////////////////////////////////
    // OBSERVABLE DATA
    ///////////////////////////////////////////////////

    self.monthly = ko.observableArray([]);
    self.years   = ko.observableArray([]);
    self.cash    = ko.observable();

    self.first_stats  = ko.observableArray([]);
    self.second_stats = ko.observableArray([]);

    self.managers = ko.observableArray();
    self.manager = ko.observable();

    self.pool_funds = ko.observableArray();
    self.user_funds = ko.observableArray();

    self.result  = ko.observable();
    self.message = ko.observable();
    self.sim_id  = ko.observable();

    self.show_loading = ko.observable(false);
    self.show_error   = ko.observable(false);

    self.section_name = ko.observable('Fund pool');
    self.section_code = ko.observable('pool');

    ///////////////////////////////////////////////////
    // CSS SELECTORS
    ///////////////////////////////////////////////////

    self.TOOLTIP = "#simulation_section .weights"
    self.CURRENT = "#simulation_section .current-composition"

    self.HISTORICAL          = "#simulation_section .historical-composition"
    self.MONTHLY_COMPOSITION = "#simulation_section .weights .distribution"

    self.BENCHMARK = "#simulation_section .benchmark .cumulative-return"

    ///////////////////////////////////////////////////
    // NOT-OBSERVABLE DATA
    ///////////////////////////////////////////////////

    self.weights = null;
    self.isins   = null;

    self.funds   = null;
    self.current_funds = null;

    self.historical_distribution = null;
    self.current_distribution = null;

    self.total_time = 0;

    self.categories = null;

    self.GREEN  = "3BB878";
    self.YELLOW = "ACD372";
    self.ORANGE = "FBAF5C";
    self.RED    = "F26C4F";

    self.category_data = {
        'EQT' : {'order': 10, 'color': self.GREEN,  'name': 'EQUITY'},
        'MIX' : {'order': 6,  'color': self.YELLOW, 'name': 'MIXED'},
        'ALT' : {'order': 5,  'color': self.YELLOW, 'name': 'ALTERNATIVE' },
        'FIX' : {'order': 2,  'color': self.ORANGE, 'name': 'FIXED INCOME' },
        'MON' : {'order': 1,  'color': self.RED,    'name': 'MONEY' }
    };

    self.transparent = ["#FFF1E0"];

    self.radius  = 25;

    ///////////////////////////////////////////////////
    // PRIVATE FUNCTIONS
    ///////////////////////////////////////////////////

    self.changeSection = function (viewModel, event) {
        var target = $(event.target);

        var code = target.attr('data-code');
        var text = target.html();

        var tabs = target.parent().siblings();
        tabs.removeClass('active');

        target.parent().addClass("active");

        if (code == 'sim') {
            // Hiding simulation results until simulation is computed
            self.sim_id(null);
            self.init_simulation();
        }

        self.section_code(code);
        self.section_name(text);
    };

    self.adapt_data_for_donuts = function () {
        var data = [];

        var date = self.weights[0].date;
        var month = date.getMonth();

        var obj = {};

        self.isins.forEach(function (f) {
            obj[f] = 0;
        });

        obj['date'] = new Date();
        obj['empty'] = 1;

        for (var i= 0; i<month;i++) {
            data.push(obj);
        }

        self.weights.forEach(function (w) {
            var ob = Object.create(w);

            ob['empty'] = 0;

            data.push(ob);
        });

        return data;
    };

    self.get_pie_css = function (data) {
        var weights = data.funds.map(function (fund) { return fund.weight });
        var only_cash = weights.indexOf(1) != -1;

        return only_cash ? "" : "arc";
    };

    self.get_colors = function (funds) {
        var categories = funds.map(function (f) { return f.category; });
        var colors = categories.map(function (cat) { return self.category_data[cat].color });

        colors = colors.concat(self.transparent);

        return d3.scale.ordinal().range(colors);
    };

    self.get_categories_colors = function () {
        var colors = self.categories.map(function (cat) { return self.category_data[cat].color });

        return d3.scale.ordinal().range(colors);
    };

    self.get_fund_name = function (isin) {
        for (var i=0; i<self.funds.length; i++) {
            var f = self.funds[i];

            if (f.isin == isin) {
                return f.name;
            }
        }

        return '';
    };

    self.get_category_percentage = function (code, distribution, round_func) {
        var percentage = 0;

        for (var i=0; i<distribution.length; i++) {
            var fund = distribution[i];

            if (self.get_category_by_isin(fund.fund) == code) {
                percentage += fund.value;
            }
        }

        return round_func(percentage) + " %";
    };


    self.get_category_name = function (code) {
        return self.category_data[code].name;
    };

    self.round_to_1 = function (num) {
        return Math.round(num*1000)/10;
    };

    self.get_category_by_isin = function (isin) {

        for (var i=0; i<self.funds.length; i++) {
            var fund = self.funds[i];

            if (fund.isin == isin) {
                return fund.category;
            }
        }

        return null;
    }

    ///////////////////////////////////////////////////
    // PRIVATE D3 FUNCTIONS
    ///////////////////////////////////////////////////

    self.create_toolkit = function (selector) {
        self.tooltip = d3.select(selector)
            .append("div")
            .style("position", "absolute")
            .style("z-index", "10")
            .style("visibility", "hidden")
            .style("font-size", "80%")
            .style("font-weight", "bold")
            .text("a simple tooltip");
    }

    self.show_equity_chart = function (element, data) {
        var margin = {top: 0, right: 20, bottom: 30, left: 25},
            width  = 770 - margin.left - margin.right,
            height = 180 - margin.top - margin.bottom;

        var x = d3.time.scale()
            .range([0, width]);

        var y = d3.scale.linear()
            .range([height, 0]);

        var xAxis = d3.svg.axis()
            .scale(x)
            .orient("bottom")
            .ticks(5);

        var yAxis = d3.svg.axis()
            .scale(y)
            .orient("left")
            .tickSize(-width)
            .ticks(3)
            .tickFormat(function(d) { return d + "%"; });

        var line = d3.svg.line()
            .x(function(d) { return x(d.date); })
            .y(function(d) { return y(d.close); });

        var area = d3.svg.area()
            .x(function(d) { return x(d.date); })
            .y0(height)
            .y1(function(d) { return y(d.close); });

        var svg = d3.select(element).append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        x.domain(d3.extent(data, function(d) { return d.date; }));
        y.domain(d3.extent(data, function(d) { return d.close; }));

        svg.append("g")
            .attr("class", "x axis")
            .style("fill", "#999")
            .attr("transform", "translate(0," + height + ")")
            .call(xAxis);

        svg.append("g")
            .attr("class", "y axis")
            .style("fill", "#999")
            .style("font-size", "11px")
            .call(yAxis)
            .append("text")
            .style("text-anchor", "end")
            .style("font-size", "11px")
            .style("fill", "#999")
            .text(Math.round(data[data.length - 1].close) + "%")

        svg.append("path")
            .attr("class", "area")
            .attr("d", area(data));

        svg.append("path")
            .attr("class", "line")
            .attr("d", line(data));
    }

    self.show_pie_chart = function (element, funds, data, round_func) {
        var width  = 180,
            height = 180,
            radius = Math.min(width, height) / 2;

        var color = self.get_colors(funds);

        var arc = d3.svg.arc()
            .outerRadius(radius - 10)
            .innerRadius(0);

        var pie = d3.layout.pie()
            .sort(null)
            .value(function(d) { return d.value; });

        var svg = d3.select(element).append("svg")
            .attr("width", width)
            .attr("height", height)
            .append("g")
            .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

        var g = svg.selectAll(".arc")
            .data(pie(data))
            .enter().append("g")
            .attr("class", "arc");

        g.append("path")
            .attr("d", arc)
            .style("fill", function(d) { return color(d.data.fund); })
            .style("cursor", "pointer")
            .on("mouseover", function(e) { self.tooltip.text(self.get_fund_name(e.data.fund)); return self.tooltip.style("visibility", "visible"); })
            .on("mousemove", function() { return self.tooltip.style("top", (d3.event.pageY-10)+"px").style("left",(d3.event.pageX+10)+"px"); })
            .on("mouseout",  function() { return self.tooltip.style("visibility", "hidden"); });

        g.append("text")
            .attr("transform", function(d) { return "translate(" + arc.centroid(d) + ")"; })
            .attr("dy", ".35em")
            .style("text-anchor", "middle")
            .style("font-weight", "bold")
            .style("cursor", "pointer")
            .style("fill", "#FFFFFF")
            .text(round_func)
            .on("mouseover", function(e) { self.tooltip.text(self.get_fund_name(e.data.fund)); return self.tooltip.style("visibility", "visible"); })
            .on("mousemove", function() { return self.tooltip.style("top", (d3.event.pageY-10)+"px").style("left",(d3.event.pageX+10)+"px"); })
            .on("mouseout",  function() { return self.tooltip.style("visibility", "hidden"); });

    };

    self.show_pie_legend = function (selector, text_func) {
        var color = self.get_categories_colors();

        color.domain(self.categories);

        var legend = d3.select(selector).append("svg")
            .attr("class", "legend")
            .attr("width", self.radius * 4)
            .attr("height", self.radius * 3)
            .selectAll("g")
            .data(color.domain())
            .enter().append("g")
            .attr("transform", function(d, i) { return "translate(0," + i * 20 + ")"; });

        legend.append("rect")
            .attr("width", 18)
            .attr("height", 18)
            .style("fill", color);

        legend.append("text")
            .attr("x", 24)
            .attr("y", 9)
            .attr("dy", ".35em")
            .text(text_func);
    }

    self.show_donuts_graph = function (selector) {
        var padding = 10;

        // Adding empty months for aligning the breakdown graph
        var data = self.adapt_data_for_donuts();

        var color = self.get_colors(self.funds);

        var arc = d3.svg.arc()
            .outerRadius(self.radius)
            .innerRadius(0);

        var pie = d3.layout.pie()
            .sort(null)
            .value(function(d) { return d.weight; });

        color.domain(self.isins);

        data.forEach(function(d) {
            d.funds = color.domain().map(function(name) {
                var weight = +d[name];

                return {name: name, weight: weight};
            });
        });

        var svg = d3.select(selector).selectAll(".pie")
            .data(data)
            .enter().append("svg")
            .attr("class", "pie")
            .attr("width", self.radius * 2)
            .attr("height", self.radius * 2)
            .append("g")
            .attr("class", self.get_pie_css)
            .attr("transform", "translate(" + self.radius + "," + self.radius + ")");

        svg.selectAll(".arc")
            .data(function(d) { return pie(d.funds); })
            .enter().append("path")
            .attr("d", arc)
            .style("cursor", "pointer")
            .style("fill", function(d) { return color(d.data.name); })
            .on("mouseover", function(e) { self.tooltip.text(self.get_fund_name(e.data.name));return self.tooltip.style("visibility", "visible"); })
            .on("mousemove", function() { return self.tooltip.style("top", (d3.event.pageY-10)+"px").style("left",(d3.event.pageX+10)+"px"); })
            .on("mouseout",  function() { return self.tooltip.style("visibility", "hidden"); });
    }

    ///////////////////////////////////////////////////
    // PUBLIC
    ///////////////////////////////////////////////////

    self.show_historical_composition = function () {
        ///////////////////////////////
        // PAINTING PIE CHART
        ///////////////////////////////
        var total = self.weights.length;

        var round = function (d) {
            return Math.round(d.data.value*1000/total)/10 + ' %';
        };

        self.show_pie_chart(self.HISTORICAL + " .time", self.funds, self.historical_distribution, round);

        /////////////////////////////
        // PAINTING LEGEND
        ////////////////////////////
        var round = function (number) { return self.round_to_1(number / self.total_time)};
        var text_legend = function(d) { return self.get_category_name(d) + ": " + self.get_category_percentage(d, self.historical_distribution, round); }

        self.show_pie_legend(self.HISTORICAL + " .legend", text_legend);
    }

    self.show_current_composition = function () {
        ///////////////////////////////
        // PAINTING PIE CHART
        ///////////////////////////////
        var round = function (d) {
            return Math.round(d.data.value*1000)/10 + ' %';
        };

        self.show_pie_chart(self.CURRENT + " .time", self.current_funds, self.current_distribution, round);

        /////////////////////////////
        // PAINTING LEGEND
        ////////////////////////////
        var round = function (number) { return self.round_to_1(number)};
        var text_legend = function(d) { return self.get_category_name(d) + ": " + self.get_category_percentage(d, self.current_distribution, round); }

        self.show_pie_legend(self.CURRENT + " .legend", text_legend);
    }

    self.show_monthly_composition = function () {
        self.show_donuts_graph(self.MONTHLY_COMPOSITION);
    }

    ///////////////////////////////////////////////////
    // PROCESSING FUNCTIONS
    ///////////////////////////////////////////////////

    self.process_equity = function (equity) {
        var parseDate = d3.time.format("%Y-%m-%d").parse;

        equity.forEach(function(d) {
            d.date = parseDate(d.date);
            d.close = +d.close;
        });

        var index=0;
        for (var i=0; i<equity.length; i++) {
            if (equity[i].close != 0) {
                index = i;
                break;
            }
        }

        equity = equity.slice(index+1, equity.length);

        self.show_equity_chart(self.BENCHMARK, equity);
    }

    self.process_stats = function (stats) {
        self.first_stats($.map(stats.slice(0, 6), function (stat) {return new Stat(stat, self);}));
        self.second_stats($.map(stats.slice(6, 15), function (stat) {return new Stat(stat, self);}));
    };

    self.process_monthly_returns = function (data) {
        var columns = Object.keys(data[0]);

        // Casting data to their proper type (numbers)
        data.forEach(function(d) {
            columns.forEach(function(col) {
                if (col != 'date') {
                    d[col] = +d[col]*100;
                }
            });
        });

        if (data[0].MaxDD == 0) {
            data.splice(0, 1)
        }

        var len = data.length;
        data.splice(len-1, len-1);

        self.monthly($.map(data, function(item) { return new MonthlyResult(item, self) }));
    }

    self.process_weights = function (data) {
        var columns = Object.keys(data[0]);

        // Casting data to their proper type
        data.forEach(function(d) {
            columns.forEach(function(col) {
                if (col != 'date')
                    d[col] = +d[col];
                else
                    d['date'] = Date.parse(d['date']).add(15).days();
            });
        });

        self.weights = data;

        var start      = data[0];
        var start_year = start.date.getFullYear();

        var end      = data[data.length-1];
        var end_year = end.date.getFullYear();

        var years = [start_year];
        var last = start_year;
        while (last < end_year) {
            years.push(last+1);
            last = years[years.length - 1];
        }

        var years = years.map(function(year) {
          return {year: year};
        });

        self.years(years);

        // Creating data structure for paint the pie chart with time distribution
        self.historical_distribution = [];
        self.historical_distribution = self.isins.map(function (fund) { return  {'fund': fund, 'value':0 }; });

        self.weights.forEach(function(d) {
            for (var i=0; i<self.isins.length; i++) {
                self.historical_distribution[i]['value']+= d[self.isins[i]]
            }
        });

        self.total_time = 0;
        for (var i=0; i<self.historical_distribution.length; i++) {
            self.total_time += self.historical_distribution[i].value;
        }

        last = self.weights.length - 1;
        var current = self.weights[last];

        self.current_distribution = [];
        self.current_funds = [];

        for (var i=0; i<self.funds.length; i++) {
            var fund   = self.funds[i];
            var weight = current[fund.isin];

            if (weight > 0) {
                self.current_distribution.push({'fund': fund.isin, 'value': weight});
                self.current_funds.push(fund);
            }
        }

        self.show_historical_composition();
        self.show_current_composition();

        self.show_monthly_composition();
    };

    self.process_funds = function (funds) {
        var ordered_funds = [];
        var repeated_categories = funds.map(function (fund) { return fund.category });

        var categories = repeated_categories.filter(function(elem, pos) {
            return repeated_categories.indexOf(elem) == pos;
        });

        for (var i=0; i<categories.length; i++) {
            for (var j=0; j<funds.length; j++) {
                if (funds[j].category == categories[i]) {
                    ordered_funds.push(funds[j])}
                }
        }

        self.funds = ordered_funds.sort(function(a, b) {
            return self.category_data[b.category].order - self.category_data[a.category].order;
        });

        self.categories = categories.sort(function(a, b) {
            return self.category_data[b].order - self.category_data[a].order;
        });

        self.isins = $.map(ordered_funds, function (fund) { return fund.isin });
    };

    self.download_simulation = function () {
        var url = "/service/simulation/" + self.sim_id();

        $.getJSON(url, function(simulation) {
            // Processing EQUITY
            var equity = d3.csv.parse(simulation['equity']);
            self.process_equity(equity);

            // Processing STATS
            var stats = d3.csv.parse(simulation['stats']);
            self.process_stats(stats);

            // Processing FUNDS
            var funds = $.parseJSON(simulation['funds']);
            self.process_funds(funds);

            // Processing MONTHLY_RETURNS
            var  monthly_return = d3.csv.parse(simulation['monthly_return']);
            self.process_monthly_returns(monthly_return);

            // Processing WEIGHTS
            var  weights = d3.csv.parse(simulation['weight']);
            self.process_weights(weights);
        });
    };

    self.init_simulation = function () {

        $.getJSON("/service/simulate/" + self.manager().code(), function(res) {
            $('svg').remove();

            if (res.status == 'ERROR') {
                self.show_error(true);
                self.message(res.message);
                return;
            }

            if (res.status == "GENERATING") {
                self.show_loading(true);
                self.show_error(false);
                setTimeout(function(){ self.init_simulation()}, 2000);
                return;
            }

            self.show_error(false);
            self.show_loading(false);
            self.result(res.result);
            self.message(res.message);

            self.create_toolkit(self.TOOLTIP);

            self.sim_id(res.id);

            self.download_simulation();
        });
    };

    self.load_pool = function () {
        $.getJSON("/service/aaa/companies/" + self.manager().code() + '/funds', function(res) {
            var funds = $.map(res, function(item) { return new Fund(item, self)});

            self.pool_funds(funds);

            $.getJSON("/service/aaa/pool", function(res) {

                for (i=0;i<res.length;i++) {
                    var isin = res[i].isin;

                    var fund = ko.utils.arrayFirst(self.pool_funds(), function(fund) {
                        return fund.isin() === isin;
                    });

                    if (fund) {
                        fund.markStar()
                    }
                }
            });
        });

    };

    self.reload = function () {
        self.load_pool();
        self.init_simulation();
    };

    self.init_managers = function () {
        $.getJSON("/service/aaa/companies", function(res) {
            var managers = $.map(res, function(item) { return new SearchParam(item, self) });

            self.managers(managers);
            self.manager(managers[0]);

            self.reload();
        });
    };

    ///////////////////////////////////////////////////
    // INIT
    ///////////////////////////////////////////////////
    
    self.init = function () {
        self.init_managers();
    };
    
    self.init();
}

