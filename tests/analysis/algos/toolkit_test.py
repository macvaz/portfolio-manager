__author__ = 'mac'

import pandas as pd

from analysis.algos import toolkit


def test_sharpe_ratio():
    ret = pd.DataFrame([0.405465, 0.287682])

    sr = toolkit.SR_annualized(ret, 0.035)

    assert round(sr, 3) == 66.032


def test_portfolio_volatility_same_weight():
    ret = pd.DataFrame(data=[[0.01, 0.01], [0.02, 0.02]], index=['2015-10-12', '2015-10-13'])

    vol = toolkit.portfolio_volatility(ret, [0.5, 0.5])

    assert round(vol, 2) == 0.01


def test_portfolio_volatility_different_weight():
    ret = pd.DataFrame(data=[[0.01, 0.01], [0.02, 0.00]], index=['2015-10-12', '2015-10-13'])

    vol = toolkit.portfolio_volatility(ret, [0.5, 0.5])

    assert round(vol, 2) == 0.00


def test_portfolio_volatility_different_weight():
    ret = pd.DataFrame(data=[[0.01, 0.01], [0.02, 0.01]], index=['2015-10-12', '2015-10-13'])

    vol = toolkit.portfolio_volatility(ret, [0.6, 0.4])

    assert round(vol, 3) == 0.004


def test_ret_portfolio():
    navs = pd.DataFrame(data=[[1, 1], [1.02, 1.03]], index=['2015-10-12', '2015-10-13'])

    ret = toolkit.ret_log_portfolio(navs)
    rounded_ret = map(lambda x: round(x, 2), ret.ix['2015-10-13'].values)

    assert rounded_ret == [0.02, 0.03]