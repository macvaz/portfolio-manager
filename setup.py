# -*- coding: utf-8 -*-

import os
from setuptools import setup


def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


setup(
    name="portfolio manager",
    version="0.0.1",
    author="Miguel Angel Cañas Vaz",
    author_email="macvaz82@gmail.com",
    description=("Quantitative investing tool for Spain-based funds"),
    license="BSD",
    keywords="quantitative funds investing tools",
    url="",
    packages=['analysis', 'web'],
    long_description=read('README.md'),
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Topic :: Utilities",
        "License :: OSI Approved :: BSD License",
    ],
)
