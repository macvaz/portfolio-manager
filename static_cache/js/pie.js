var width = 960,
  height = 500,
  radius = Math.min(width, height) / 2;

var color = d3.scale.ordinal()
  .range(["#3BB304", "#94E070", "#F7F76A", "#F5A947", "#F50202"]);

var arc = d3.svg.arc()
  .outerRadius(radius - 10)
  .innerRadius(0);

var pie = d3.layout.pie()
  .sort(null)
  .value(function(d) { return d.count; });

var svg = d3.select("#content").append("svg")
  .attr("width", width)
  .attr("height", height)
  .append("g")
  .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");
  
var data = 
  [
    {"action": "Comprar",       "count": 400},
    {"action": "Sobreponderar", "count": 300},
    {"action": "Mantener",      "count": 600},
    {"action": "Infraponderar", "count": 300},
    {"action": "Vender",        "count": 200},
  ]

var g = svg.selectAll(".arc")
  .data(pie(data))
  .enter().append("g")
  .attr("class", "arc");

g.append("path")
  .attr("d", arc)
  .style("fill", function(d) { return color(d.data.action); });

g.append("text")
  .attr("transform", function(d) { return "translate(" + arc.centroid(d) + ")"; })
  .attr("dy", ".35em")
  .attr("class", "link")
  .style("text-anchor", "middle")
  .text(function(d) { return d.data.action; })
  .on("click", function(d){
  	alert("hola")
  });
