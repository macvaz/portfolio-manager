# Prerequisites

This project relies on **docker** for physical deployment. It also requires an  
**PostgreSQL** database available from the docker container running the application.

# Architecture

There are 2 different components:
  - **Django-based application** (both webapp and crawler). Developed in Python. Library 
  dependencies are managed through pip. Building docker image deals with all packaging issues 
  generating a production-ready assembly.
  - **RDBMS** . PostgreSQL 10.21. The db server exports PostgreSQL port (5432) to Django's container.

# Build software

```bash
cd docker
./build
```

# Run software

Before running the Django container, ensure external PostgreSQL service is available within container. From the 
host, execute:

```bash
deploy/bin/postgre/start.sh
```

Run portfolio-manager docker container:

```bash
cd docker
./run
```


