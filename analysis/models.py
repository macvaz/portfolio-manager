'''
Created on 09/04/2013

@author: mac
'''

from django.db import models

from django.contrib import admin

import numpy as np

from constants import constants


class ISIN(models.Model):
    """ Initial data we use to import funds from MorningStar """

    isin = models.CharField(max_length=30, primary_key=True)
    ric = models.CharField(max_length=30)
    created_fund = models.BooleanField()

    def __unicode__(self):
        return self.isin

    class Meta:
        db_table = 'isin'


class Subcategory(models.Model):
    name = models.CharField(max_length=200)

    def __unicode__(self):
        return self.short_name()

    def to_json(self):
        return {
            'name': self.short_name()
        }

    def short_name(self):
        for prefix in constants.SUBCATEGORY_PREFIX:
            if self.name.startswith(prefix):
                return self.name[len(prefix):]

        return self.name

    class Meta:
        db_table = 'subcategory'


class AssetManager(models.Model):
    name = models.CharField(max_length=200)

    def __unicode__(self):
        return self.name

    def to_json(self):
        return {
            'name': self.name,
            'code': self.id
        }

    class Meta:
        db_table = 'asset_manager'


class Fund(models.Model):
    """ Main information of the fund """
    isin = models.ForeignKey(ISIN, primary_key=True)

    name = models.CharField(max_length=200)
    category = models.CharField(max_length=3, choices=constants.FUND_CATEGORY)
    ms_category = models.CharField(max_length=200, null=True)

    subcategory = models.ForeignKey(Subcategory, null=True)
    company = models.ForeignKey(AssetManager)

    currency = models.CharField(max_length=3, choices=constants.FUND_CURRENCY)
    fee = models.FloatField(null=True)

    def __unicode__(self):
        return unicode(self.isin)

    def get_url(self):
        return constants.FUND_URL.format(self.isin.ric)

    def short_category_name(self):
        for prefix in constants.SUBCATEGORY_PREFIX:
            if self.category.startswith(prefix):
                return self.ms_category[len(prefix):]

        return self.ms_category

    def to_json(self):
        return {
            'name': self.name,
            'isin': self.isin,
            'category': self.category,
            'currency': self.currency
        }

    class Meta:
        db_table = 'fund'


class PerformanceMetrics(models.Model):
    """ Stores latest metrics computed for each fund """

    fund = models.ForeignKey(Fund, primary_key=True)
    date = models.DateField()

    performance_1d = models.FloatField(null=True)
    performance_2d = models.FloatField(null=True)

    performance_1w = models.FloatField(null=True)
    performance_2w = models.FloatField(null=True)

    performance_1m = models.FloatField()
    performance_3m = models.FloatField()
    performance_6m = models.FloatField()
    performance_9m = models.FloatField(null=True)
    performance_12m = models.FloatField(null=True)
    performance_ytd = models.FloatField(null=True)

    sharpe_1m = models.FloatField(null=True)
    sharpe_3m = models.FloatField(null=True)
    sharpe_6m = models.FloatField(null=True)
    sharpe_6m = models.FloatField(null=True)
    sharpe_9m = models.FloatField(null=True)
    sharpe_12m = models.FloatField(null=True)

    volatility_1m = models.FloatField()
    volatility_3m = models.FloatField()
    volatility_6m = models.FloatField()
    volatility_9m = models.FloatField(null=True)
    volatility_12m = models.FloatField(null=True)

    beta_6m = models.FloatField(null=True)
    beta_up_6m = models.FloatField(null=True)
    beta_down_6m = models.FloatField(null=True)

    corr_6m = models.FloatField(null=True)

    def __unicode__(self):
        return unicode(self.fund)

    class Meta:
        db_table = 'performance_metrics'

    def to_json(self):
        return {
            'date': self.date.strftime("%d/%b"),

            'performance_1d': self.performance_1d,
            'performance_2d': self.performance_2d,

            'performance_1w': self.performance_1w,
            'performance_2w': self.performance_2w,

            'performance_1m': self.performance_1m,
            'performance_3m': self.performance_3m,
            'performance_6m': self.performance_6m,
            'performance_9m': self.performance_9m,
            'performance_12m': self.performance_12m,
            'performance_ytd': self.performance_ytd,

            'sharpe_1m': self.sharpe_1m,
            'sharpe_3m': self.sharpe_3m,
            'sharpe_6m': self.sharpe_6m,
            'sharpe_9m': self.sharpe_9m,
            'sharpe_12m': self.sharpe_12m,

            'volatility_1m': self.volatility_1m,
            'volatility_3m': self.volatility_3m,
            'volatility_6m': self.volatility_6m,
            'volatility_9m': self.volatility_9m,
            'volatility_12m': self.volatility_12m,

            'beta_6m': self.beta_6m,
            'beta_up_6m': self.beta_up_6m,
            'beta_down_6m': self.beta_down_6m,

            'corr_6m': self.corr_6m if not np.isnan(self.corr_6m) else None,

            'name': self.fund.name,
            'isin': self.fund.isin_id,
            'url': self.fund.get_url(),

            'currency': constants.get_currency_symbol(self.fund.currency),
            'ms_category': self.fund.short_category_name(),
            'category': self.fund.category,

            'subcategory_id': self.fund.subcategory_id,
        }


class Ranking(models.Model):
    """ Fund ranking computed from momentum, sharpe ratio, volatility and signals """

    fund = models.ForeignKey(Fund, primary_key=True)

    mom_cur = models.FloatField()
    mom_prev = models.FloatField()
    mov_avg = models.FloatField()

    sr = models.FloatField()
    vol = models.FloatField()
    ret = models.FloatField()

    nav = models.FloatField()

    ranking = models.FloatField()

    def __unicode__(self):
        return self.fund

    def to_json(self):
        return {
            'url': self.fund.get_url(),
            'name': self.fund.name,
            'currency': constants.get_currency_symbol(self.fund.currency),

            'mom_cur': round(self.mom_cur, 4),
            'mom_prev': round(self.mom_prev, 4),

            'sr': round(self.sr, 2),
            'vol': round(self.vol, 4),
            'ret': round(self.ret, 4),

            'nav': round(self.nav, 2),

            'ranking': round(self.ranking, 2)
        }

    class Meta:
        db_table = 'ranking'

# Registering in Admin
admin.site.register(Fund)
admin.site.register(ISIN)
admin.site.register(PerformanceMetrics)
admin.site.register(Ranking)
admin.site.register(AssetManager)
admin.site.register(Subcategory)
