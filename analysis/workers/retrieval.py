import gevent
import gevent.monkey
from gevent.queue import JoinableQueue

""" Monkey-patching socket connections in order to schedule another user-level thread avoiding
context-switching penalties """
gevent.monkey.patch_socket()
gevent.monkey.patch_ssl()

queue = JoinableQueue()

wrong_urls = []
wrong_data = []

errors = []
empty = []

ALL_WORKERS = 15
BY_ISIN_WORKERS = 5

###########
# WORKERS #
###########


def simple_worker(fn):
    while True:
        # models.ISIN
        isin = queue.get()

        try:
            fn(isin)
        except Exception, e:
            print "ERROR: {0}".format(isin.isin)
            errors.append((isin.isin, e))
        finally:
            queue.task_done()


def crawler_worker(fn):
    count = 0

    while True:
        fund = queue.get()

        try:
            fund = fn(fund)

            count += 1

            if (count % 10) == 0:
                print fund.name[0]

        except Exception as e:
            print "ERROR: {0}".format(fund.isin)
            errors.append((fund, e))
        finally:
            queue.task_done()


def downloader_worker(get_df, process_df, benchmark_navs):
    """
    Downloader worker skeleton parametrizable with 'get_df' and 'process_df' functions
    @param get_df: Function
    @param process_df: Function
    """
    count = 0

    while True:
        fund = queue.get()

        try:
            df = get_df(fund)

            count += 1

            if (count % 10) == 0:
                print fund.name[0].upper()

            if df is None or df.empty:
                empty.append(fund)
                continue

            process_df(df, fund, benchmark_navs)
        except Exception, e:
            errors.append((fund, e))
        finally:
            queue.task_done()


def run(num_workers, worker, items):
    """
    Starts 'num_workers' workers and blocks until all items are processed
    @param num_workers: int
    @param worker: function
    @param items: [models]
    @return: None
    """
    global errors, empty

    errors = []
    empty = []
    greenlets = []

    for i in range(num_workers):
        greenlets.append(gevent.spawn(worker))

    for item in items:
        queue.put(item)

    queue.join()

    gevent.killall(greenlets)

    if num_workers == BY_ISIN_WORKERS:
        return

    print "################"
    print "EMPTY"
    print "################"

    for fund in empty:
        print 'NA: ' + unicode(fund)

    print "################"
    print "WRONG"
    print "################"

    for (item, e) in errors:
        print item, e
