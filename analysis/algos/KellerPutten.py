from toolkit import *

"""
Keller&Putten Algo
Generalized Momentum and Flexible Asset Allocation (FAA): An Heuristic Approach

http://papers.ssrn.com/sol3/papers.cfm?abstract_id=2193735

A Absolute ROC
V Volatility
C Correlation

"""


class KellerPutten():

    def __init__(self, cash_fund):
        self.capital = 100000
        self.cash_fund = cash_fund

    ##################
    # PUBLIC METHODS #
    ##################

    def backtest(self, id, matrix, lookback, top, store):

        ##################
        # INITIALIZATION
        ##################
        self.raw_matrix = matrix

        prices = self.raw_matrix.dropna()

        ###############################
        # COMPUTING FACTORS: ROC, A and VOL
        ###############################

        ret = ROC(prices, 1)

        a = self._A(prices, 4)
        v = rolling_vol(ret, lookback)

        ##################################################################
        # RANKING FUNDS PERFORMANCE BASED ON COMPUTATIONS FOR EACH MONTH
        ##################################################################

        # Skipping end_dates with less than "lookback" previous data
        end_month_indexes = filter(lambda x: x >= lookback, [ret.index.get_loc(end) for end in a.index.values])

        # Initializing weights matrix
        weights = pd.DataFrame(0.0, index=a.index, columns=self.raw_matrix.columns)

        for i in end_month_indexes:
            # computing ending month date of the current iteration
            month_end = ret.index.values[i]

            c = corr(ret, lookback, i, month_end)

            rank_a = rank(-get_row_as_dataframe(a, month_end))
            rank_v = rank(get_row_as_dataframe(v, month_end))
            rank_c = rank(c)

            total = 1.2*rank_a + 0.5*rank_v + 0.5*rank_c

            total_rank = rank(total)

            print_rank("rank A",   rank_a)
            print_rank("rank VOL", rank_v)
            print_rank("rank COR", rank_c)
            print_rank("TOTAL",    total_rank)

            # top funds
            top_funds = self._get_top_funds(total_rank, top)

            # absolute momentum filter
            top_funds_performance = get_row_as_dataframe(a, month_end).ix[:, top_funds]

            print_rank("TOP", top_funds_performance)

            invest_in_funds = self._get_positive_performers(top_funds_performance)

            if len(invest_in_funds) < top and self.cash_fund not in invest_in_funds:
                invest_in_funds.append(self.cash_fund)

            self._assign_weights(weights, month_end, invest_in_funds, self.cash_fund, top)

        backtest(prices, weights, ret, self.capital, store, id)

    ###################
    # PRIVATE METHODS
    ###################

    # Computes discrete ROC (Rate of Change), aggregated by months with a "lookback" number of values
    def _A(self, prices, lookback):
        index = month_endpoints(prices)

        month_prices = prices.reindex(index=index)

        return ROC_ar(month_prices, lookback).dropna()

    def _get_top_funds(self, total_rank, top):
        return total_rank[total_rank <= top].dropna(axis=1, how='all').columns.tolist()

    def _assign_weights(self, weights, month_end, invest_in_funds, cash_fund, top):
        num_funds = float(len(invest_in_funds))

        for fund in invest_in_funds:
            if fund == cash_fund:
                weights.ix[month_end, [fund]] = float((top - num_funds + 1)/top)
            else:
                weights.ix[month_end, [fund]] = 1.0/top

    def _get_positive_performers(self, df):
        return df[df >= 0].dropna(axis=1,how='all').dropna(axis=1,how='all').columns.tolist()