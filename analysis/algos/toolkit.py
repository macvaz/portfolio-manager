__author__ = 'mac'

from numpy import nan, log
from math import sqrt

from pandas.io.data import read_csv

from pandas.core.datetools import *
from pandas.stats.moments import rolling_std

import pandas as pd
import numpy as np

from dateutil.parser import parse
from analysis.constants import constants

from datetime import timedelta
import datetime

################################
# SIMPLE PERFORMANCE FUNCTIONS #
################################


def ytd_navs(navs):
    # Dealing with year changes
    last_nav_datetime = navs.index.values[-1]
    year = str(last_nav_datetime).split("-")[0]
    year_start = datetime.date(int(year), 1, 1)

    ytd_navs = navs.ix[year_start:]
    return ytd_navs


def ret_log_series(series, months=None):
    """
    Compute log returns of a pd.Series
    :param series: pd.Series
    :param months: int
    :return: float
    """
    series = get_temporal_window(series, months)
    return log(series/series.shift(1))


def ret_simple_series(series, months=None):
    """
    Computes aritmethic return of a pandas Series
    :param serie: Series
    :return: float
    """
    series = get_temporal_window(series.dropna(), months)
    return (series[-1] - series[0])/series[0]


def ret_log_portfolio(df, months=None):
    """
    Return the dataframe of the log returns from a dataframe of the NAVs
    :param df: DataFrame
    :param months: int
    :return: DataFrame
    """
    df = get_temporal_window(df, months)
    return np.log(df) - np.log(df.shift(1))


def vol_annualized(ret):
    """
    Return annualized volatility
    @param ret: pd.DataFrame daily returns
    @return: Series
    """
    return sqrt(252)*ret.std()


def period_vol(ret):
    """
    Return volatility of a given period (adjusted by period length)
    @param ret: pd.DataFrame daily returns
    @return: Series
    """
    return sqrt(ret.size)*ret.std()


def SR_annualized(ret, risk_free):
    """
    Compute annualized sharpe ratio from a pandas DataFrame
    @param ret: pd.DataFrame
    @param risk_free: Number
    @return: pd.Series
    """
    annualized_ret = ret.mean()*252
    vol = sqrt(252) * ret.std()

    return (annualized_ret - risk_free) / vol


def SR(navs, risk_free):
    """
    Compute sharpe ratio from a pandas DataFrame without annualizing (based on the timeframe)
    @param navs: pd.DataFrame
    @param risk_free: Number
    @return: pd.Series
    """
    acc_return = ret_simple_series(navs)
    ret = ret_log_series(navs)
    vol = sqrt(ret.size)*ret.std()

    return (acc_return - risk_free) / vol


#########################
# ECONOMETRIC FUNCTIONS #
#########################


# Compute volatility in a given lookback period
def rolling_vol(returns, lookback):
    return sqrt(252)*rolling_std(returns, lookback).dropna()


# Arithmetic Rate of Change. Asymmetric when going up and down
def ROC_ar(prices, lookback):
    return prices / prices.shift(lookback) - 1


# Logarithmic of Continuously Compounded ROC. Symmetric when going up and down
def ROC(prices, lookback):
    return log(prices / prices.shift(lookback)).dropna()


# Returns a vector with the average correlation between columns in the matrix
def corr(matrix, lookback, ix, month_end):
    matrix = matrix.ix[ix-lookback:ix+1]
    cor_matrix = matrix.corr()

    columns = cor_matrix.columns
    size = len(columns)

    for i in range(size):
        cor_matrix.ix[i, i] = nan

    result = []
    for i in range(size):
        row = cor_matrix.ix[i].dropna()
        size = len(row)

        result.append(sum(row) / size)

    return get_row_as_dataframe(matrix, month_end, result)


def betas(df, portfolio, benchmark, months=None):
    """
    Return computed betas for a given portfolio against a benchmark
    :param df: Daily returns of the portfolio and the benchmark
    :param portfolio: [String] List of isins of the portfolio (excluding benchmark)
    :param benchmark: Benchmark ISIN
    :param months:
    :return:
    """
    df = get_temporal_window(df, months)
    return [compute_beta(df, benchmark, isin) for isin in portfolio]


def compute_beta(df, benchmark, isin):
    benchmark_variance = df[benchmark].var()
    beta = df[isin].cov(df[benchmark])/benchmark_variance
    if np.isnan(beta):
        return None
    return beta


def portfolio_volatility(df, weights, months=None):
    """
    Return volatility of the portfolio in %
    :param df: dataframe with the daily returns
    :param weights: [] with the weights of each asset
    :param months
    :return: float
    """
    days = 1 if months is None else months*constants.MARKET_DAYS_PER_MONTH
    covariance_matrix = df.cov()
    w = np.array(weights)

    return np.sqrt(days)*np.sqrt(w.T.dot(covariance_matrix).dot(w))

######################
# RANKING FUNCTIONS  #
######################


# Rank highest the biggest value
def rank(df):
    return df.rank(axis=1)

##################
# DATE FUNCTIONS #
##################


def get_temporal_window(df, months):
    """
    Return a dataframe with the the data of tha last 'months' months
    :param df: pd.DataFrame | pd.Series
    :param months: int
    :return: DataFrame | Series
    """
    if months:
        last_date = df.index[-1]
        return df.ix[rewind_time(last_date, months*constants.DAYS_PER_MONTH):]
    return df


def get_data_from_date(df, date):
    return df.ix[date:]


def rewind_time(date, days):
    """
    Rewind date 'days' behind
    @param date: date.datetime
    @param days: integer
    @return: date.datetime
    """
    return date - timedelta(days=days)


def month_endpoints(matrix):
    start = matrix.index[0]
    end = matrix.index[-1]

    end_candidates = [date.date() for date in pd.DatetimeIndex(start=start, end=end, freq=BMonthEnd()).to_datetime()]
    ends = []

    for end_candidate in end_candidates:
        while True:
            try:
                # Existing date in prices?
                matrix.ix[end_candidate]
                ends.append(end_candidate)
                break
            except Exception, e:
                # No date in prices => decreasing a day and trying again
                end_candidate = end_candidate - timedelta(days=1)

    return ends


def get_date(date):
    return to_datetime(date)


def year_endpoints(matrix):
    month_ends = month_endpoints(matrix)

    ends = []
    for end in month_ends:
        if end.month == 12:
            ends.append(end)

    last = month_ends[-1]
    if last.month != 12:
        ends.append(last)

    return ends

########################
# DATAFRAME FUNCTIONS  #
########################


def get_row_as_dataframe(df, label, data=None):
    if not data:
        data = df.ix[label]

    return pd.DataFrame([data], [label], df.columns)


def fillback_dataframe_with_previous(matrix):
    matrix.apply(fillback_serie_with_previous, axis=0)


def fillback_serie_with_previous(serie):
    last = np.nan

    for i in range(len(serie)):
        element = serie.ix[i]

        if pd.notnull(element):
            last = element
            continue

        if pd.isnull(element) and pd.notnull(last):
            serie.ix[i] = last

    return serie


def list2matrix(m, l, rows, columns):
    data = [tuple(m[n * l:l*(n + 1)]) for n in range(0, len(m) / l)]

    return pd.DataFrame(data, index=rows, columns=columns)


def read_fund_csv_from_string(content):
    names = ['Date', 'Open', 'High', 'Low', 'Close', 'Volume', 'Adjusted']

    try:
        df = read_csv(StringIO(content), index_col=0, header=None, skip_footer=1,
                      parse_dates=False, sep=';', skiprows=1, names=names)

        df['index'] = [parse(date).date() for date in df.index]

        df = df.set_index('index')

        return df
    except Exception as e:
        return None


def build_df_1_series(index, serie, cols):
    df = pd.DataFrame(index=index, data=serie, columns=cols)

    return df.sort()

################
# BACKTESTING  #
################


def backtest(prices, weights, ret, capital, store, id):
    # Computing date range
    start_date = weights.index[0]
    end_date = weights.index[-1]

    # Monitoring performance
    shares = pd.DataFrame(0.0, index=ret.index, columns=weights.columns)
    shares.ix[weights.index] = weights

    # Normalizing prices and adjusting number of shares of each fund to the weights
    shares = (capital/prices) * weights

    # Moving date on day to the future and filling all NaN with the value of the start day of the month
    future = shares.shift(1)
    fillback_dataframe_with_previous(future)
    future = future.fillna(0)

    # Computing cash per day
    cash = capital - (future*prices.shift(1)).sum(axis=1)

    # Moving shares_1 one day in the past
    past = future.shift(-1)

    # Detecting trades
    tstart = (future != past) & (past != 0)
    tend = (future != past) & (future != 0)

    trade = (tstart | tend).fillna(False)

    index = trade.apply(any, 1).shift(1).fillna(False)

    total_cash = nan * cash
    total_cash[index] = cash[index]

    fillback_serie_with_previous(total_cash)

    total_cash = total_cash.fillna(0)

    total_ret = ((total_cash + (future*prices).sum(axis=1)) /
                 (total_cash + (future*prices.shift(1)).sum(axis=1)) - 1).fillna(0)

    w = future * prices.shift(1).div(total_cash + (future * prices.shift()).sum(axis=1))
    w = w.fillna(0)

    equity = (1 + total_ret).cumprod()

    years = len(pd.DatetimeIndex(start=start_date, end=end_date, freq='D'))/365.0
    cagr = np.power(equity.ix[-1], float(1/years))-1

    equity_ret = ROC(equity, 1)

    vol = vol_annualized(equity_ret)
    sr = SR_annualized(equity_ret, constants.RISK_FREE_RATE)

    # Computing monthly returns
    month_ends_label = month_endpoints(equity)
    month_ends_index = [equity.index.get_loc(label) for label in month_ends_label]

    year_ends_label = year_endpoints(equity)
    year_ends_index = [equity.index.get_loc(label) + 1 for label in year_ends_label]
    years = [str(year.year) for year in year_ends_label]

    nr = len(year_ends_label) + 1

    # Creating monthly returns empty table
    rows = years + ['Avg']
    columns = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec', 'Year', 'MaxDD']

    monthly_returns_df = pd.DataFrame(index=rows, columns=columns, dtype=np.float64)

    year_index = [1] + year_ends_index

    # compute yearly profit and drawdown
    for i in range(1, len(year_index)):
        iequity = equity.ix[year_index[i-1]-1:year_index[i]]

        monthly_returns_df.ix[i-1, 'Year'] = iequity[-1] / iequity[0] - 1
        monthly_returns_df.ix[i-1, 'MaxDD'] = min(iequity / iequity.cummax() - 1)

    # compute monthly profit
    monthly_returns = (equity[month_ends_index].diff() / equity[month_ends_index]).dropna().values.tolist()

    start_month = min(month_ends_label).month
    end_month = max(month_ends_label).month

    best = max(monthly_returns)
    worst = min(monthly_returns)

    first_non_zero = 0
    for i in xrange(len(monthly_returns)):
        if monthly_returns[i] != 0:
            first_non_zero = i
            break

    avg = np.mean(monthly_returns[first_non_zero:])

    monthly_returns = [nan] * start_month + monthly_returns + [nan] * (12-end_month)

    monthly_returns = list2matrix(monthly_returns, 12, years, columns[0:12])

    monthly_returns_df.update(monthly_returns)

    # compute averages
    monthly_returns_df.ix['Avg'] = monthly_returns_df.mean()

    num_months = len(weights.index.values)
    num_years = num_months/12

    stats = pd.Series([best, worst, avg, cagr, sr, vol, num_months, num_years],
                      ['best', 'worst', 'avg', 'cagr', 'sr', 'vol', 'num_months', 'num_years'])

    # Preparing pandas objects for storage
    equity.name = 'close'
    equity.index.name = 'date'

    stats.name = 'value'
    stats.index.name = 'name'

    monthly_returns_df.index.name = 'date'
    weights.index.name = 'date'

    # Marking simulation as downloaded!
    store.remove(id)

    # Storing simulation results into HDF
    # Its not possible to store non-pandas objects => creating a Series with all float objects
    store.put('{0}/{1}'.format(id, 'stats'), stats)

    store.put('{0}/{1}'.format(id, 'equity'), equity)
    store.put('{0}/{1}'.format(id, 'monthly_return'), monthly_returns_df)
    store.put('{0}/{1}'.format(id, 'weight'), weights)