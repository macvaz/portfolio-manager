# -*- coding: utf-8 -*-

'''
Created on 24/03/2013

@author: mac
'''

import os

PROJECT_HOME = os.environ.get("PROJECT_HOME")
FUND_URL = "http://www.morningstar.es/es/funds/snapshot/snapshot.aspx?id={0}"

RISK_FREE_RATE = 0

DAYS_PER_MONTH = 365/12
MARKET_DAYS_PER_MONTH = 252/12
VALIDATION_COEFICIENT = 0.9

# GRAPH BENCHMARKS
# Amundi MSCI World, Amundi MSCI SP500
GRAPH_BENCHMARKS = ['LU0996182563'] #, 'LU0996179007']

# BETA BENVHMARk
# EUROSTOXX 50
BENCHMARK = 'ES0114754031'
BENCHMARK_COLUMN_NAME = "BENCHMARK"

SUBCATEGORY_PREFIX = [
    "Alt - ",
    "RF ",
    "Mixtos ",
    "Inmobiliario - ",
    "Mercado Monetario ",
    "Fecha Objetivo ",
    "Trading - ",
    "RV ",
    "Materias Primas - "
]

FUND_CATEGORY = (
    ('EQT', 'Equity'),
    ('FIX', 'Fixed Income'),
    ('MIX', 'Mixed'),
    ('ALT', 'Alternative'),
    ('MON', 'Money'),
    ('RE',  'Real Estate'),
    ('GUA', 'Guaranteed'),
    ('PRO', 'Protected'),
    ('COM', 'Commodities'),
    ('OTH', 'Others'),
    ('INF', 'Inflation-Linked'),
    ('TRA', 'Trading'),
    ('DAT', 'Target Date'),
    ('EXT', 'External')
)

FUND_CURRENCY = (
    ('EUR', 'Euro'),
    ('USD', 'US Dollar'),
    ('GBP', 'GB Pound'),
)

CURRENCY_SYMBOL = (
    ('EUR', '€'),
    ('USD', '$'),
    ('GBP', '£'),
)

SCREENER_RULE = (
    ('0',  'SR > 1.0'),
    ('1',  'SR 6m > 1.0'),
    ('2',  'SR 3m > 1.0'),
    ('3',  'SR 1m > 1.0'),
    ('4',  '% 1m > 0'),
    ('5',  '% 1d > 0'),
    ('6',  '% 2d > 0'),
    ('7',  '% 3d > 0'),
    ('8',  '% 1w > 0'),
    ('9',  '% 2d,1w > 0'),
    ('10', 'All'),
)

####################
# PUBLIC FUNCTIONS #
####################


def get_categories():
    return [{'code': code, 'name': name} for (code, name) in FUND_CATEGORY]


def get_currencies():
    return [{'code': code, 'name': name} for (code, name) in FUND_CURRENCY]


def get_screener_rules():
    return [{'code': code, 'name': name} for (code, name) in SCREENER_RULE]


def get_currency_symbol(requested_code):
    for (code, symbol) in CURRENCY_SYMBOL:
        if code == requested_code:
            return symbol

    return ""
