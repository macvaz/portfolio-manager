import requests

from analysis.db import db
from analysis.models import ISIN

from BeautifulSoup import BeautifulSoup
from urlparse import urlparse, parse_qs

import time

####################
# PUBLIC FUNCTIONS #
####################


def import_all():
    # There are 35 categories
    categories = range(1, 36)

    while len(categories) > 0:
        errors = []

        print 'DOWNLOADING CATEGORIES: {0}'.format(categories)

        for category in categories:
            isins = _download_isins_by_category(URL, PAGE_SIZE, category)

            if isins is not None:
                _store_isins(isins['rows'])
                print "downloaded category {0} with {1} funds".format(category, len(isins['rows']))
            else:
                print "Error in category %s" % category
                errors.append(category)

            time.sleep(5)

        categories = errors

#####################
# PRIVATE FUNCTIONS #
#####################


def _download_isins_by_category(url, page_size, category):
    try:
        return requests.get(url.format(page_size, category)).json()
    except requests.exceptions.ConnectionError:
        return None
    except ValueError:
        return None


def _store_isins(isins):
    for isin_data in isins:
        isin = isin_data['cell'][8]
        link = isin_data['cell'][0]

        link_doc = BeautifulSoup(link, convertEntities=BeautifulSoup.HTML_ENTITIES)

        href = link_doc.find('a')['href']
        qs = urlparse(href).query

        ric = parse_qs(qs)['id'][0]

        importer = db.get_or_create(ISIN, isin=isin, created_fund=False, ric=ric)
        importer.save()

####################
# PRIVATE DATA     #
####################

URL = 'https://lt.morningstar.com/api/rest.svc/security_list/xgnfa0k0aw/FOEUR%24%24ALL_522?ModuleId=76&' \
      'currencyId=EUR&locale=en-US&languageId=es-ES&viewId=Snapshot&InstitutionUniverseId=&' \
      'onlyInstitutionfunds=&ColumnList=,PdfButton,Name,AdministratorCompanyName,CustomBuyFee,' \
      'CustomDepositaryFee,CustomMinimumPurchaseAmount,BuyButton,CustomIsFavourite,' \
      'ISIN&outputtype=compactjsonformat&sortCriteria=LegalName&CustomCategoryId3={1}' \
      '&_search=false&PageSize={0}&page=1&sidx=ReturnM12&sord=desc'

PAGE_SIZE = 900
