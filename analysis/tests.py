'''
Created on 30/11/2012

@author: macvaz
'''

from tools import downloader

from numpy import nan
from numpy.testing import assert_array_almost_equal

from algos import toolkit
from datetime import datetime

from django.test import TestCase

import pandas as pd
import os

#############################
# FINANCIAL TIMES CRAWLING  #
#############################


class TestFinancialTimesCrawling(TestCase):

    def test_parse_date(self):
        date = downloader._parse_ft_date("August 02", 2013, None)

        self.assertEqual(date, datetime(2013, 8, 2).date())

    def test_year_change(self):
        date = downloader._parse_ft_date("December 02", 2013, 1)

        self.assertEqual(date, datetime(2012, 12, 2).date())

    def test_parse_ft_year(self):
        year = downloader._parse_ft_year("As of market close Aug 09 2013. ")

        self.assertEqual(year, 2013)

############
# Toolkit  #
############


class TestToolkit(TestCase):
    N = 10
    serie1 = pd.Series(range(1, N+1), index=range(1, N+1))
    serie2 = pd.Series(range(2, N+2), index=range(1, N+1))
    serie3 = pd.Series(range(3, N+3), index=range(1, N+1))

    matrix1 = pd.DataFrame({'1': serie1, '2': serie2, '3': serie3})

    serie4 = pd.Series([1, 4, 5, 6])
    serie5 = pd.Series([20, 4, 2, 2])
    serie6 = pd.Series([4, -2, -2, 4])

    matrix2 = pd.DataFrame({'1': serie4, '2': serie5, '3': serie6})

    serie7 = pd.Series([1, nan, 5, nan])
    serie8 = pd.Series([1, nan, 5, nan])
    serie9 = pd.Series([4, nan, -2, nan])

    matrix3 = pd.DataFrame({'1': serie7, '2': serie8, '3': serie9})

    ROC_RESULT = [1.098612, 0.693147, 0.510826, 0.405465, 0.336472, 0.287682, 0.251314, 0.22314355131420976]

    def test_ROC(self):
        roc = toolkit.ROC(self.serie1, 2).values

        assert_array_almost_equal(roc, self.ROC_RESULT)

    def test_fillback(self):
        toolkit.fillback_dataframe_with_previous(self.matrix3)

        assert_array_almost_equal(self.matrix3.ix[1], self.matrix3.ix[0])