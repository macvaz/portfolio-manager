__author__ = 'mac'

from django.core.management.base import BaseCommand

from datetime import datetime, date

from analysis.constants import constants
from analysis.db import db
from analysis.tools import downloader

from web.db import queries

import string
import time


class Command(BaseCommand):

    def handle(self, *args, **options):
        started_at = datetime.now()

        today = date.today()
        end = today.strftime("%Y-%m-%d")
        start = date(today.year - 3, today.month, today.day)

        benchmark = db.get_fund_by_isin(constants.BENCHMARK)

        for letter in string.ascii_lowercase:
            print("Listing funds starting by: {0}".format(letter))

            letter_funds = db.get_funds_by_initial_letter(letter)
            benchmark_navs = downloader.benchmark_navs(benchmark, start, end)

            downloader.compute_metrics(benchmark_navs, letter_funds, start, end)
            
            print("Sleeping 200s")
            time.sleep(200)
            print("Running again")

        print("######################################")
        print("FINISHED!! in {0}".format(datetime.now() - started_at))
        print("######################################")
