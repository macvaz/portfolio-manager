__author__ = 'mac'

from django.core.management.base import BaseCommand
from analysis.importers import morning_star

import datetime


class Command(BaseCommand):

    def handle(self, *args, **options):
        start = datetime.datetime.now()

        morning_star.import_all()

        end = datetime.datetime.now()

        print "######################################"
        print "FINISHED!! in {0}".format(end - start)
        print "######################################"