__author__ = 'mac'

from django.core.management.base import BaseCommand

from analysis.crawlers import morning_star, external

import datetime


class Command(BaseCommand):

    def handle(self, *args, **options):
        start = datetime.datetime.now()

        external.create_funds()
        #morning_star.crawl_funds()

        end = datetime.datetime.now()

        print "######################################"
        print "FINISHED!! in {0}".format(end - start)
        print "######################################"
