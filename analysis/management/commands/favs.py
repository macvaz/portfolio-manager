__author__ = 'mac'

from django.core.management.base import BaseCommand

from datetime import datetime, date

from analysis.constants import constants
from analysis.db import db
from analysis.tools import downloader

from web.db import queries


class Command(BaseCommand):

    def handle(self, *args, **options):
        started_at = datetime.now()

        today = date.today()
        end = today.strftime("%Y-%m-%d")
        start = date(today.year - 3, today.month, today.day)

        favs = queries.get_all_favorite_funds()

        benchmark = db.get_fund_by_isin(constants.BENCHMARK)
        benchmark_navs = downloader.benchmark_navs(benchmark, start, end)
        
        downloader.compute_metrics(benchmark_navs, favs, start, end)
        downloader.store_navs_in_files([benchmark], start, end)
        downloader.store_navs_in_files(favs, start, end)

        print("######################################")
        print("FINISHED!! in {0}".format(datetime.now() - started_at))
        print("######################################")
