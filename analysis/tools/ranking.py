from analysis.db import db
from analysis.algos import toolkit
from analysis.constants import constants
from analysis.workers import ranking

from analysis.models import Ranking

import pandas as pd
import numpy as np

CUR_NAV = 'CUR_NAV'
CUR_MA = 'CUR_MA'
CUR_MOM = 'CUR_MOM'

PREV_NAV = 'PREV_NAV'
PREV_MA = 'PREV_MA'
PREV_MOM = 'PREV_MOM'

VOL = 'VOL'
SR = 'SR'
RET = 'RET'

RANK = 'RANKING'

##########
# PUBLIC #
##########


def rank(store, lookback, subperiod_size):
    isins = store.keys()
    worker = lambda: ranking.ranking_worker(store, lookback, lambda df: rank_funds(df, subperiod_size))

    ranking.run(30, worker, isins)


def rank_funds(df, subperiod_size):
    factors = _compute_factors(df, subperiod_size)
    _apply_rank_by_category(factors)

###########
# PRIVATE #
###########


def _get_ith_valid(series, index):
    try:
        return series.dropna().iget(index)
    except Exception, e:
        return np.nan


def _compute_factors(df, length):
    """
    Compute factors for a given dataframe and two periods. SR, VOL and RET are computed for the complete dataframe.
    @param df: pd.DataFrame
    @param length: int. Size of the subperiod
    @return: pd.DataFrame
    """
    subperiod_df = df.tail(length)

    # Creating result DataFrame
    res = pd.DataFrame(index=df.columns)

    res[CUR_NAV] = df.apply(lambda series: _get_ith_valid(series, -1))
    res[CUR_MA] = df.mean()
    res[CUR_MOM] = (res[CUR_NAV] - res[CUR_MA])/res[CUR_MA]

    res[PREV_NAV] = subperiod_df.apply(lambda series: _get_ith_valid(series, -1))
    res[PREV_MA] = subperiod_df.mean()
    res[PREV_MOM] = (res[PREV_NAV] - res[PREV_MA])/res[PREV_MA]

    ret = toolkit.ret_log_series(df)

    res[VOL] = toolkit.vol_annualized(ret)
    res[SR] = toolkit.SR(df, constants.RISK_FREE_RATE)

    first_nav = df.apply(lambda series: _get_ith_valid(series, 0))

    res[RET] = (res[CUR_NAV] - first_nav)/first_nav

    return res


def _apply_rank_by_category(df):
    for isin in df.index.values:
        row = df.ix[isin]

        if np.isnan(row[SR]) or np.isnan(row[VOL]):
            print "Problem computing ranking for: ", isin
            return

        try:
            fund = db.get_fund_by_isin(isin)
            ranking = db.get_or_create(Ranking, fund=fund)
        except Exception, e:
            print "Error with ISIN: ", isin
            continue

        ranking.mom_cur = row[CUR_MOM]
        ranking.mom_prev = row[PREV_MOM]
        ranking.mov_avg = row[CUR_MA]

        ranking.sr = row[SR]
        ranking.vol = row[VOL]
        ranking.ret = row[RET]

        ranking.nav = row[CUR_NAV]

        ranking.ranking = _apply_rank(fund.category, row)

        ranking.save()


def _apply_rank(category, row):
    rank = None
    if category == 'FIX':
        rank = _rank_FIX(row)

    rank = _rank_EQT(row)

    if not rank or np.isnan(rank):
        return 0
    else:
        return rank


def _rank_EQT(row):
    return (row[RET]*25 + row[CUR_MOM]*50 + row[PREV_MOM]*50 + (row[CUR_MOM] - row[PREV_MOM])*50 + row[SR])/row[VOL]


def _rank_FIX(row):
    return row[RET]*25 + row[CUR_MOM]*50 + row[PREV_MOM]*50 + (row[CUR_MOM] - row[PREV_MOM])*50 + \
        row[SR] - 100*abs(row[VOL] - 0.04)
