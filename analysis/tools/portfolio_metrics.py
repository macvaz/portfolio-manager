__author__ = 'mac'

from analysis.algos import toolkit


def volatility(navs, w, months=None):
    """

    :param navs: pd.DataFrame
    :param w:
    :param months:
    :return:
    """
    ret_df = toolkit.ret_log_portfolio(navs, months)
    return toolkit.portfolio_volatility(ret_df, w, 12)


def correlation(navs, w, months, benchmark):
    """

    :param navs: pd.DataFrame
    :param w:
    :param months:
    :param benchmark: Column name of the benchmark
    :return:
    """
    def computeReturnUdf(row):
        return sum([row[i]*w[i] for i in range(len(navs.columns) - 1)])

    ret_df = toolkit.ret_log_portfolio(navs, months)
    equity_curve = ret_df.apply(computeReturnUdf, axis=1)
    return equity_curve.corr(ret_df[benchmark])
