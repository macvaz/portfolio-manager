from analysis.db import db
from analysis.db import csv
from analysis.tools import metrics
from analysis.workers import retrieval

from datetime import datetime
import pandas as pd
import requests
import json

URL = """
http://tools.morningstar.es/api/rest.svc/timeseries_price/2nhcdckzon?
id={0}%5D2%5D1%5D&
currencyId={1}&
idtype=Morningstar&
frequency=daily&
startDate={2}&
endDate={3}&
performanceType=&
outputType=COMPACTJSON
"""

####################
# PUBLIC FUNCTIONS #
####################


def benchmark_navs(benchmark, start, end):
    """
    :param benchmark: Fund
    :param start: String with "yyyy-mm-dd" format
    :param end: String with "yyyy-mm-dd" format
    :return: pd.DataFrame
    """
    return _parse_navs(benchmark, start, end)


def compute_metrics(benchmark_navs, funds, start, end):
    """
    :param funds: [Fund]
    :param benchmark_navs: pd.DataFrame
    :param start: String with "yyyy-mm-dd" format
    :param end: String with "yyyy-mm-dd" format
    :return: None
    """
    get_df = lambda fund: _parse_navs(fund, start, end)
    worker = lambda: retrieval.downloader_worker(get_df, metrics.store_metrics_in_db, benchmark_navs)

    retrieval.run(retrieval.ALL_WORKERS, worker, funds)


def store_navs_in_files(funds, start, end):
    """
    :param funds: [Fund]
    :param benchmark_navs: pd.DataFrame
    :param start: String with "yyyy-mm-dd" format
    :param end: String with "yyyy-mm-dd" format
    :return: None
    """
    get_df = lambda fund: _parse_navs(fund, start, end)
    worker = lambda: retrieval.downloader_worker(get_df, csv.store_dataframe_in_file, None)

    retrieval.run(retrieval.ALL_WORKERS, worker, funds)


#####################
#  PRIVATE METHODS  #
#####################

def _parse_navs(fund, start, end):
    isin = db.get_isin_by_fund(fund)
    url = _compute_url(isin.ric, fund.currency, start, end)
    text = requests.get(url).text
    data = [[_parse_date(row[0]), row[1]] for row in json.loads(text)]
    navs = [item[1] for item in data]
    index = [item[0] for item in data]
    df = pd.DataFrame(data=navs, index=index, columns=[isin.isin])
    df.index.name = 'DATE'
    return df


def _compute_url(ric, currency, start, end):
    return URL.format(ric, currency, start, end).replace("\n", "")


def _parse_date(ms):
    return datetime.fromtimestamp(ms/1000).date()
