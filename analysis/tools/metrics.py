__author__ = 'mac'

import pandas as pd
import numpy as np

from decimal import Decimal

from analysis.db import db, csv
from analysis.constants import constants

from analysis.models import PerformanceMetrics

from analysis.algos import toolkit

# DEFAULT METRICS
MONTHS = [1, 3, 6, 9, 12]
WEEKS = [1, 2]
DAYS = [1, 2]


METRICS_CONFIG = [(d, 'd', d) for d in DAYS] + [(w, 'w', w*5) for w in WEEKS] + \
                 [(m, 'm', int(round(m * constants.DAYS_PER_MONTH))) for m in MONTHS]


def compute_metrics(df, fund, benchmark_navs):
    if df is None or df.empty:
        return

    metrics = _compute_from_df(df, benchmark_navs)
    _store_fund_metrics(fund.isin, metrics)


def store_metrics_in_db(df, fund, benchmark_navs):
    if df is None:
        return

    compute_metrics(df, fund, benchmark_navs)


#####################
# PRIVATE FUNCTIONS #
#####################


def _compute_from_df(df, benchmark_navs):
    metrics = \
        [_compute_metric(df, benchmark_navs, *metric_config) for metric_config in METRICS_CONFIG] + [_compute_ytd(df)]
    return filter(None, metrics)


def _compute_ytd(df):
    ytd_navs = toolkit.ytd_navs(df)
    serie = ytd_navs.iloc[:, 0]
    ytd = toolkit.ret_simple_series(serie, None) * 100
    return {"performance_ytd": ytd, "period": "ytd"}


def _compute_metric(navs_df, benchmark_navs_df, amount, period, days):
    isin = navs_df.columns[0]
    navs = navs_df[isin]
    positive_series = navs[navs > 0]
    last_date = navs.index[-1]
    last_nav = navs[-1]

    if period == 'm':
        # Slicing by a range of dates
        navs = navs.ix[toolkit.rewind_time(last_date, days):]
    else:
        # Slicing by a range of available values (no date involved)
        navs = navs[-(days + 1):]

    if len(navs) < 2:
        print "ERROR: Insufficent navs"
        return None

    ret = toolkit.ret_log_series(navs)
    vol = 100 * toolkit.vol_annualized(ret)
    sr = toolkit.SR_annualized(ret, constants.RISK_FREE_RATE)

    acc_return = 100 * toolkit.ret_simple_series(navs)

    beta = None
    beta_up = None
    beta_down = None
    corr = None
    if period == 'm' and amount == 6:
        beta, beta_up, beta_down = _compute_betas(navs_df, benchmark_navs_df)
        corr = _compute_correlation(navs_df, benchmark_navs_df)

    return _build_response(
        amount,
        period,
        acc_return,
        vol,
        sr,
        last_date,
        last_nav,
        beta,
        beta_up,
        beta_down,
        corr,
        len(positive_series))

def _compute_betas(navs, benchmark_navs):

    if benchmark_navs is None:
        return None, None, None

    if navs.columns == benchmark_navs.columns:
        return 1, 1, 1

    portfolio = [navs.columns[0]]
    benchmark = benchmark_navs.columns[0]

    navs_df = benchmark_navs.join(navs)

    ret_df = toolkit.ret_log_portfolio(navs_df, 6)
    upward_df = ret_df[ret_df[benchmark] > 0]
    downward_df = ret_df[ret_df[benchmark] < 0]

    beta = toolkit.betas(ret_df, portfolio, constants.BENCHMARK)[0]
    beta_up = toolkit.betas(upward_df, portfolio, constants.BENCHMARK)[0]
    beta_down = toolkit.betas(downward_df, portfolio, constants.BENCHMARK)[0]

    return beta, beta_up, beta_down


def _compute_correlation(navs, benchmark_navs):
    if benchmark_navs is None:
        return None

    if navs.columns == benchmark_navs.columns:
        return 1

    ret_navs = toolkit.ret_log_portfolio(navs, 6)
    ret_benchmark_df = toolkit.ret_log_portfolio(benchmark_navs, 6)
    ret_benchmark_df['fund'] = ret_navs

    return ret_benchmark_df.corr().ix[0, 1]


def _round_4(float):
    if pd.isnull(float):
        return None

    return round(Decimal(float), 4)


def _build_response(amount, type, acc_return, vol, sr, date, nav, beta, beta_up, beta_down, corr, size):
    return {
        'amount': amount,
        'period': type,
        'return': _round_4(acc_return),
        'vol': _round_4(vol),
        'date': date,
        'nav': _round_4(nav),
        'beta': beta,
        'beta_up': beta_up,
        'beta_down': beta_down,
        'corr': corr,
        'size': size,

        'sharpe_ratio': 0 if np.isinf(sr) else _round_4(sr)
    }


def _store_fund_metrics(isin, metrics_list):
    fund = db.get_fund_by_isin(isin)

    model = db.get_or_create(PerformanceMetrics, fund=fund)

    for metric in metrics_list:
        period = metric['period']

        if period == 'ytd':
            model.performance_ytd = metric['performance_ytd']
            continue

        amount = metric['amount']
        date = metric['date']
        ret = metric['return']
        beta = metric['beta']
        beta_up = metric['beta_up']
        beta_down = metric['beta_down']
        corr = metric['corr']
        size = metric['size']

        model.date = date

        if (period == 'm' and amount > 6 and size >= amount * constants.MARKET_DAYS_PER_MONTH * constants.VALIDATION_COEFICIENT) or \
            (period != 'm') or (period == 'm' and amount <= 6):
            setattr(model, 'performance_{}{}'.format(amount, period), ret)
        else:
            setattr(model, 'performance_{}{}'.format(amount, period), None)

        # Checking period type
        # Storing sharpe ratios and volatilities only for month periods (day periods are not long enough)
        if period == 'm':
            setattr(model, 'sharpe_{}{}'.format(amount, period), metric['sharpe_ratio'])
            setattr(model, 'volatility_{}{}'.format(amount, period), metric['vol'])
            if amount == 6:
                model.beta_6m = beta
                model.beta_up_6m = beta_up
                model.beta_down_6m = beta_down
                model.corr_6m = corr

    model.save()