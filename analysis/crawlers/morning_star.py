from BeautifulSoup import BeautifulSoup
from analysis.workers import retrieval

from analysis.models import ISIN, Fund, AssetManager, Subcategory
from analysis.constants import constants
from analysis.db import db

import requests

#########
# STATE #
#########

headers = {
    "Accept": "text/html",
    "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:17.0) Gecko/17.0 Firefox/17.0",
    "Host": "www.morningstar.es"
}

####################
# PUBLIC FUNCTIONS #
####################


def crawl_funds():
    """
    A pool of async workers is setup. Each worker handles several connections during it lifetime
    @return: None
    """
    isins = ISIN.objects.all()

    worker = lambda: retrieval.simple_worker(_crawl_fund)

    print "###########################"
    print "CRAWLING FROM MORNING STAR"
    print "###########################"

    retrieval.run(5, worker, isins)

#####################
# PRIVATE FUNCTIONS #
#####################


def _crawl_fund(isin):
    """
    Crawl fund by its isin
    @param isin: models.ISIN
    @return: None
    """

    general_url = constants.FUND_URL.format(isin.ric)
    management_url = general_url + "&tab=4"

    try:
        general_html = requests.get(general_url, headers=headers, verify=False).text
        management_html = requests.get(management_url, headers=headers, verify=False).text
    except Exception as e:
        print "Error connecting to Url : " + general_url
        print (e)
        return

    general_doc = BeautifulSoup(general_html, convertEntities=BeautifulSoup.HTML_ENTITIES)
    general_doc = general_doc.find('div', {'id': 'mainContentDiv'})

    management_doc = BeautifulSoup(management_html, convertEntities=BeautifulSoup.HTML_ENTITIES)
    management_doc = management_doc.find('div', {'id': 'managementManagementFundCompanyDiv'})

    # Parsing general tab
    name_div = general_doc.find("div", {'class': 'snapshotTitleBox'})
    name = name_div.find("h1").text

    overview = general_doc.find('div', {'id': 'overviewQuickstatsDiv'})
    currency = overview.findAll('tr')[1].find('td', {'class': 'line text'}).text[0:3]
    ms_category = overview.findAll('tr')[3].find('td', {'class': 'line value text'}).find('a').text
    fee = overview.findAll('tr')[8].find('td', {'class': 'line text'}).text.replace('%', '').replace(',', '.')

    # Parsing management tab
    management_company = management_doc.find('tr').findAll('td')[1].text

    try:
        _store_fund(name, currency, management_company, ms_category, isin, fee)
    except Exception as e:
        print e


def _store_fund(name, currency, management_company, ms_category, isin, fee):
    """
    Save Fund in database. Creating if don't exist Asset_Manager and Subcategory
    :param name: String
    :param currency: String
    :param management_company: String
    :param ms_category: String
    :param isin: models.ISIN
    :return: None
    """

    asset_manager = db.get_or_create(AssetManager, name=management_company)
    asset_manager.name = management_company

    asset_manager.save()

    subcategory = db.get_or_create(Subcategory, name=ms_category)

    subcategory.save()

    fund = db.get_or_create(Fund, isin=isin)
    fund.currency = currency
    fund.name = name
    fund.company = asset_manager
    fund.isin = isin
    fund.ms_category = ms_category
    fund.subcategory = subcategory
    fund.category = _compute_category(ms_category)
    fund.fee = fee

    fund.save()

    isin.created_fund = True
    isin.save()


def _compute_category(ms_category):
    if not ms_category:
        return

    if ms_category.startswith("RV"):
        return "EQT"

    if ms_category.startswith("RF"):
        return "FIX"

    if ms_category.startswith("Alt"):
        return "ALT"

    if ms_category.startswith("Mercado"):
        return "MON"

    if ms_category.startswith("Inmobiliario"):
        return "RE"

    if ms_category.startswith("Mixtos"):
        return "MIX"

    if ms_category.startswith("Materias"):
        return "COM"

    if ms_category.startswith("Fecha Objetivo"):
        return "DAT"

    if ms_category.startswith("Capital"):
        return "PRO"

    if ms_category.startswith("Otros"):
        return "OTH"

    if ms_category.startswith("Garantizados"):
        return "GUA"

    if ms_category.startswith("Trading"):
        return "TRA"

    print "Problem decoding category : {0}".format(ms_category)