# -*- coding: utf-8 -*-

__author__ = 'mac'

from analysis.db import db
from analysis.models import ISIN, Fund, AssetManager, Subcategory

EXTERNAL_FUNDS = [
    {
        'name': 'Smart Social SICAV',
        'isin': 'ES0176062000',
        'ric': 'F00000VPI6'
    },
    {
        'name': 'Lierde SICAV',
        'isin': 'ES0158457038',
        'ric': 'F000001YK4'
    },
    {
        'name': 'DWS Top Dividende LD',
        'isin': 'DE0009848119',
        'ric': 'F0GBR04G6Z'
    },
    {
        'name': 'Amundi Fds Index Eq Emerging Mkts Ae-C',
        'isin': 'LU0996177134',
        'ric': 'F00000T8H9'
    },
    {
        'name': 'Amundi Is Sp 500 Ae-C',
        'isin': 'LU0996179007',
        'ric': 'F00000T7PZ'
    },
    {
        'name': 'Amundi Is Msci World Ae-C',
        'isin': 'LU0996182563',
        'ric': 'F00000T66U'
    },
    {
        'name': 'Groupama Avenir Euro I',
        'isin': 'FR0000990038',
        'ric': 'F0GBR04JXV'
    },
    {
        'name': 'Algar Global Fund FI',
        'isin': 'ES0140963002',
        'ric': 'F00000VYOH'
    },
    {
        'name': 'Japan Deep Value FI',
        'isin': 'ES0156673008',
        'ric': 'F00000XJRM'
    },
    {
        'name': 'Bestinver Internacional FI',
        'isin': 'ES0114638036',
        'ric': 'F0GBR04PSY'
    },
    {
        'name': 'Cobas Seleccion FI',
        'isin': 'ES0124037005',
        'ric': 'F00000XX1F'
    },
    {
        'name': 'Incometric Equam Global Value A',
        'isin': 'LU0933684101',
        'ric': 'F00000V8XG'
    },
    {
        'name': 'Magallanes Microcaps Europe B FI',
        'isin': 'ES0159202011',
        'ric': 'F00000YPKL'
    },
    {
        'name': 'Robeco BP US Premium Equities F €',
        'isin': 'LU0832430747',
        'ric': 'F00000OWHQ'
    },
    {
        'name': 'Vanguard US Opportunities Fund',
        'isin': 'IE00B03HCY54',
        'ric': 'F0000001NV'
    },
    {
        'name': 'Esfera I - Baelo Patrimonio FI',
        'isin': 'ES0110407097',
        'ric': 'F0000107JE'
    },
    {
        'name': 'Fundsmith Equity Feeder T Eur Inc',
        'isin': 'LU0690375422',
        'ric': 'F00000OHVX'
    },
    {
        'name': 'Horos Value Iberia',
        'isin': 'ES0146311008',
        'ric': 'F000010KY5'
    },
    {
        'name': 'Horos Value Internacional',
        'isin': 'ES0146309002',
        'ric': 'F000010KY6'
    },
    {
        'name': 'DNB Fund Technology retail A',
        'isin': 'LU0302296495',
        'ric': 'F000000JQ3'
    },
    {
        'name': 'Franklin Technology Fund A(acc)EUR',
        'isin': 'LU0260870158',
        'ric': 'FOGBR05JFY'
    },
    {
        'name': 'BSF European Opportunities Extension Fund A2 EUR',
        'isin': 'LU0313923228',
        'ric': 'F000000IK5'
    },
    {
        'name': 'Lombard Odier Funds - Generation Global (EUR) NA',
        'isin': 'LU0428702855',
        'ric': 'F000002XZI'
    },
    {
        'name': 'Ishares Emerging Mkts Eq Idx -Lu- A2 Usd',
        'isin': 'LU0836513183',
        'ric': 'F00000P5RU'
    },
    {
        'name': 'Hermes Global Emerging Mkts R Eur Acc',
        'isin': 'IE00B3NFBQ59',
        'ric': 'F00000NA73'
    },
    {
        'name': 'Fiera Capital Global Equity C Acc',
        'isin': 'IE00BZ60KC09',
        'ric': '0P0001D33V'
    },
    {
        'name': 'Impassive Wealth FI',
        'isin': 'ES0147897005',
        'ric': 'F00001155A'
    },
    {
        'name': 'Magna Mena G Eur Acc',
        'isin': 'IE00BFTW8Y10',
        'ric': 'F00000TNDR'
    },
    {
        'name': 'Franklin U.S. Opportunities Fund W(Acc)EUR-H1',
        'isin': 'LU1586276476',
        'ric': 'F00000YWNM'
    },
    {
        'name': 'Esfera III Vetusta Inversión FI',
        'isin': 'ES0131445092',
        'ric': 'F000014BWK'
    },
    {
        'name': 'Xtrackers ShortDAX Daily Swap UCITS ETF 1C',
        'isin': 'LU0292106241',
        'ric': '0P000094C6'
    },
    {
        'name': 'Esfera II - Gesfund Aqua FI',
        'isin': 'ES0131444020',
        'ric': 'F00000YPJA'
    },
    {
        'name': 'River Patrimonio FI',
        'isin': 'ES0173985005',
        'ric': 'F000011HM0'
    },
    {
        'name': 'Numantia Patrimonio',
        'isin': 'ES0173311103',
        'ric': 'F00000VYOL'
    },
    {
        'name': 'MS Global Opportunity Fund i',
        'isin': 'LU0552385295',
        'ric': 'F00000LNTQ'
    },
    {
        'name': 'Vanguard Global Bond Index Fund Investor EUR Hedged Accumulation',
        'isin': 'IE00BGCZ0933',
        'ric': 'F00000VIK1'
    },
    {
        'name': 'Amundi Index FTSE EPRA NAREIT Global AE-C',
        'isin': 'LU1328852659',
        'ric': 'F00000WPBR'
    },
    {
        'name': 'Vanguard US 500 Stock Index Inv EUR Acc',
        'isin': 'IE0032620787',
        'ric': 'F0GBR04G0F'
    },
    {
        'name': 'Morgan Stanley | Global Opportunity Fund I',
        'isin': 'LU0834154790',
        'ric': 'F00000OVNY'
    },
    {
        'name': 'Andromeda Value',
        'isin': 'ES0173311079',
        'ric': 'F00000VYOI'
    },
    {
        'name': 'Comgest Growth Europe Opportunities EUR R Acc',
        'isin': 'IE00BD5HXJ66',
        'ric': 'F00000QTGM'
    },
    {
        'name': 'Vanguard US Government Index (USD)',
        'isin': 'IE0007471695',
        'ric': 'F0GBR04V7F'
    },
    {
        'name': 'Kronos FI',
        'isin': 'ES0156572002',
        'ric': 'F00000ZJ1W'
    },
    {
        'name': 'iShares US Index Fund (IE) Institutional Acc USD',
        'isin': 'IE00B1W56J03',
        'ric': 'F000005PKU'
    },
    {
        'name': 'Vanguard VTI (Total Stock Index)',
        'isin': 'US9229087690',
        'ric': '0P00002DAJ'
    },
    {
        'name': 'iShares TLT (Long Term US Bonds)',
        'isin': 'IE00BSKRJZ44',
        'ric': '0P000159YP'
    },
    {
        'name': 'iShares Gold Trust (IAU)',
        'isin': 'US4642851053',
        'ric': '0P00002DCW'
    },
    {
        'name': 'Icaria Cartera Permanente',
        'isin': 'ES0147491007',
        'ric': 'F00000ZJ1V'
    },
    {
        'name': 'Esfera Seasonal Quant Multistrategy R FI',
        'isin': 'ES0131462097',
        'ric': 'F00000ZL6J'
    },
    {
        'name': 'Vanguard 20+ Year Euro Treasury Index Fund EUR Acc',
        'isin': 'IE00B246KL88',
        'ric': 'F000001GFI'
    },
    {
        'name': 'Alger Small Cap Focus Fund Class A US',
        'isin': 'LU1339879758',
        'ric': 'F00000WYC5'
    },
    {
        'name': 'Esfera I - Quant USA FI',
        'isin': 'ES0110407055',
        'ric': 'F00000ZN9M'
    },
    {
        'name': 'True Capital FI',
        'isin': 'ES0180782007',
        'ric': 'F000015RLT'
    },
    {
        'name': 'MFS European Value Fund A1 EUR',
        'isin': 'LU0125951151',
        'ric': 'F0GBR04GNL'
    },
    {
        'name': 'Vanguard Global Stock Market EUR',
        'isin': 'IE00B03HCZ61',
        'ric': 'F0GBR04SKK'
    },
    {
        'name': 'Baillie Gifford Worldwide US Equity Growth',
        'isin': 'IE00BF0D7Y67',
        'ric': 'F00000ZE5R'
    },
    {
        'name': 'MS Europe Opportunity Fund A',
        'isin': 'LU1387591305',
        'ric': 'F00000X14G'
    },
    {
        'name': 'MS Global Opportunity Ah Eurh',
        'isin': 'LU0552385618',
        'ric': 'F00000X14G'
    },
    {
        'name': 'Baillie Gifford WW Glb Alp A USD Acc',
        'isin': 'IE00B88CSH68',
        'ric': 'F00000P8LA'
    },
    {
        'name': 'MS Asia Opportunity Fund A',
        'isin': 'LU1378878430',
        'ric': 'F00000X0M8'
    },
    {
        'name': 'MS Global Bond Fund Z',
        'isin': 'LU0360476583',
        'ric': 'F000002NR1'
    },
    {
        'name': 'Esfera I - Nubeo FI',
        'isin': 'ES0110407121',
        'ric': 'F000014OS7'
    },
    {
        'name': 'Valentum FI',
        'isin': 'ES0182769002',
        'ric': 'F00000SRXL'
    },
    {
        'name': 'Vanguard Global Small-Cap USD Acc',
        'isin': 'IE00B42LF923',
        'ric': 'F000005MD4'
    },
    {
        'name': 'Threadneedle (Lux) - Global Smaller Companies AE',
        'isin': 'LU0570870567',
        'ric': 'F00000M1MH'
    },
    {
        'name': 'MS US Advantage Fund A',
        'isin': 'LU0225737302',
        'ric': 'F0GBR0645X'
    },
    {
        'name': 'MS US Growth Fund AH',
        'isin': 'LU0266117414',
        'ric': 'F000000253'
    },
    {
        'name': 'True Value Small Caps B FI',
        'isin': 'ES0179555018',
        'ric': 'F000014YJ2'
    },
    {
        'name': 'MS Global Bond Fund A',
        'isin': 'LU0073230426',
        'ric': 'f0gbr04e8z'
    },
    {
        'name': 'Alger Dynamic Opportunities Long Short',
        'isin': 'LU1232088200',
        'ric': 'F00000VYA7'
    },
    {
        'name': 'Lazard Convertible Global RC EUR',
        'isin': 'FR0010858498',
        'ric': 'F00000GWUJ'
    },
    {
        'name': 'Eleva Absolute Return Europe A1',
        'isin': 'LU1331971769',
        'ric': 'F00000ZEXG'
    },
    {
        'name': 'BlackRock World Energy Fund A2 EUR Hedged',
        'isin': 'LU0326422176',
        'ric': 'F000000RNM'
    },
    {
        'name': 'BlackRock Euro Bond A2',
        'isin': 'LU0050372472',
        'ric': 'F0GBR04ASI'
    },
    {
        'name': 'Amundi Volatility World A EUR Hgd',
        'isin': 'LU0442406889',
        'ric': 'F0000045HF'
    },
    {
        'name': 'Vanguard Eurozone Inflation-Linked Bond',
        'isin': 'IE00B04GQQ17',
        'ric': 'F000002K7S'
    },
    {
        'name': 'Amundi Volatility Euro A EUR (C)',
        'isin': 'LU0272941971',
        'ric': 'F0000002EC'
    },
    {
        'name': 'Pictet TR - Atlas',
        'isin': 'LU1433232854',
        'ric': 'F00000Y840'
    },
    {
        'name': 'Pictet TR - Mandarin',
        'isin': 'LU0496443531',
        'ric': 'F00000JTB9'
    },
    {
        'name': 'Pictet TR - Atlas Titan',
        'isin': 'LU2206557170',
        'ric': 'F000015JW4'
    },
    {
        'name': 'Goldman Sachs - Commodity Index',
        'isin': 'LU0397155978',
        'ric': 'F00000H2TI'
    },
    {
        'name': 'Baillie Gifford Worldwide - Long Term Global Growth',
        'isin': 'IE00BK5TW727',
        'ric': 'F000014BLX'
    },
    {
        'name': 'Baillie Gifford Worldwide - Health Innovation',
        'isin': 'IE00BN7HTK74',
        'ric': 'F000015Z2J'
    },
    {
        'name': 'Baillie Gifford Worldwide - US Equity Growth F',
        'isin': 'IE00BK5TWD80',
        'ric': 'F000014BLZ'
    },
    {
        'name': 'Esfera I - Value',
        'isin': 'ES0110407063',
        'ric': 'F0000107JH'
    },
    {
        'name': 'Icaria Capital Dinámico',
        'isin': 'ES0147474003',
        'ric': 'F000011559'
    },
    {
        'name': 'Esfera - Robotics',
        'isin': 'ES0131462139',
        'ric': 'F000014BWL'
    },
    {
        'name': 'Esfera II - Azagala FI',
        'isin': 'ES0131444111',
        'ric': 'F000013WNP'
    },
    {
        'name': 'Indexa RV Mixta Internacional 75 FI',
        'isin': 'ES0148181003',
        'ric': 'F000013GR7'
    },
    {
        'name': 'Oricalco FI',
        'isin': 'ES0107696017',
        'ric': 'F000014GIJ'
    },
    {
        'name': 'MyInvestor Nasdaq 100',
        'isin': 'ES0165265002',
        'ric': 'F00001836C'
    },
    {
        'name': 'MyInvestor Ponderado Economía Mundial',
        'isin': 'ES0184894006',
        'ric': 'F00001836D'
    },
    {
        'name': 'MyInvestor SP500 Equiponderado',
        'isin': 'ES0165242001',
        'ric': 'F00001836E'
    },
    {
        'name': 'Vanguard Global Short-Term Bond Index Fund EUR Hedged',
        'isin': 'IE00BH65QP47',
        'ric': 'F00000T4PK'
    },
    {
        'name': 'Vanguard Emerging Markets Stock Index Fund EUR Acc',
        'isin': 'IE0031786696',
        'ric': 'F00000T1HU'
    },
    {
        'name': 'Myinvestor Cartera Permanente FI',
        'isin': 'ES0156572002',
        'ric': 'F00000ZJ1W'
    },
    {
        'name': 'DWS Invest CROCI World LC',
        'isin': 'LU1769941003',
        'ric': 'F00001100Z'
    },
    {
        'name': 'Vanguard Global Short-Term Bond Index Fund USD Hedged Acc',
        'isin': 'IE00BH65QN23',
        'ric': 'F00000SK5Y'
    },
    {
        'name': 'Vanguard Emerging Markets Bond Fund (EUR)',
        'isin': 'IE00BKLWXS37',
        'ric': 'F000014IA0'
    },
    {
        'name': 'Vanguard U.S. Investment Grade Credit Index Fund EUR Acc',
        'isin': 'IE00B04GQT48',
        'ric': 'F0000022KV'
    },
    {
        'name': 'Vanguard U.S. Government Bond Index Fund EUR Hedged Acc',
        'isin': 'IE0007471471',
        'ric': 'F0GBR052E1'
    },
    
]


def create_funds():
    asset_manager = db.get_or_create(AssetManager, name="EXTERNAL")
    asset_manager.save()

    subcategory = db.get_or_create(Subcategory, name="EXTERNAL")
    subcategory.save()

    for f in EXTERNAL_FUNDS:
        isin = db.get_or_create(ISIN, isin=f['isin'], ric=f['ric'], created_fund=True)
        isin.save()

        fund = db.get_or_create(Fund, isin=isin)
        fund.currency = 'EUR'
        fund.name = f['name']
        fund.company = asset_manager
        fund.isin = isin
        fund.ms_category = 'External'
        fund.subcategory = subcategory
        fund.category = 'EXT'

        fund.save()
