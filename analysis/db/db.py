__author__ = 'mac'

from analysis.models import Fund, ISIN

from itertools import chain


def get_all_funds():
    return Fund.objects.all().order_by("name")

def get_funds_by_initial_letter(letter):
    return Fund.objects.filter(name__istartswith=letter)

def get_funds_without_performance():
    return Fund.objects.raw("select fund.* from fund where fund.isin_id NOT IN (select fund_id from performance_metrics) order by fund.name;")


def get_or_create(cls, **kwargs):
    """
    Return an existing db model of type 'cls' or a new model of type 'cls' if not existing in db
    @param cls: Class
    @param kwargs: All arguments needed to identify the model in database
    @return: model of type 'cls'
    """
    try:
        model = cls.objects.get(**kwargs)
    except cls.DoesNotExist:
        model = cls(**dict((k, v) for (k, v) in kwargs.items() if '__' not in k))

    return model


def get_funds_by_benchmark(benchmark, currency):
    return Fund.objects.filter(subcategory__benchmark=benchmark, currency=currency)


def get_isin_by_fund(fund):
    return ISIN.objects.get(isin=fund.isin)


def get_funds_by_isins(isins):
    """
    Return all funds identified by 'isins'
    @param isins: list[String]
    @return: list[Fund]
    """
    return Fund.objects.filter(isin__in=isins)


def get_fund_by_isin(isin):
    """
    Return the fund identified by 'isin'
    @param isins: String
    @return: Fund
    """
    return Fund.objects.get(isin=isin)


def get_funds_by_subcategory(subcategory):
    return Fund.objects.filter(subcategory = subcategory)