__author__ = 'mac'

import os
import pandas as pd

DATA_FOLDER = "{0}/data".format(os.environ.get("PROJECT_HOME"))


def store_dataframe_in_file(df, fund, benchmark_navs):
    """

    :param df:
    :param fund:
    :param benchmark_navs:
    :return:
    """
    file_path = "{0}/{1}.csv".format(DATA_FOLDER, fund.isin)

    final_df = df
    #if os.path.exists(file_path):
    #    final_df = pd.concat([read_nav_file(fund.isin), df]).drop_duplicates().reset_index(drop=True)

    final_df.to_csv(file_path)


def read_nav_file(isin, name):
    file_path = "{0}/{1}.csv".format(DATA_FOLDER, isin)
    csv = pd.read_csv(file_path, index_col=0, parse_dates=[0])

    if name is not None:
        csv.columns = [name]

    return csv


def read_navs(isins, benchmark_isin, benchmark_column_name):
    # Reversing 'isins' list due to using reduce (foldl)
    navs = [read_nav_file(isin, None) for isin in isins[::-1]]

    if benchmark_isin is not None:
        navs = [read_nav_file(benchmark_isin, benchmark_column_name)] + navs

    return reduce(lambda x, acc: acc.merge(x, left_index=True, right_index=True), navs)


def read_navs_several_benchmarks(isins, benchmarks):
    all_isins = isins + benchmarks
    # Reversing 'isins' list due to using reduce (foldl)
    navs = [read_nav_file(isin, None) for isin in all_isins[::-1]]

    return reduce(lambda x, acc: acc.merge(x, left_index=True, right_index=True), navs)

